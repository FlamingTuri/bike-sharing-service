const mongoose = require('mongoose');
const config = require('../config');
const dbConnString = 'mongodb://' + config.db.user + ':' + config.db.password + '@' + config.db.host + '/' + config.db.defaultDatabase;

mongoose.connect(dbConnString, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(function() {
    console.log ('Successfully connected to: ' + dbConnString);
}, function (err) {
    console.log ('Unable to connect to: ' + dbConnString + '\n' + err);
});
