module.exports = {
	"secret": "zio-carmelitano",
	"server": {
		"address": "localhost",
		"port": 8443
	},
	"client": {
		"virtualPath": "/app",
		"physicalPath": "../client/dist/client"
	},
	"db" : {
		"host": "ds249623.mlab.com:49623",
        "user": "admin",
        "password": "bike_sharing1",
		"defaultDatabase": "bikesharing"
	}
};