const modelMapper = require('../utils/modelMapper');
const emitter = require('../utils/emitter');
const constants = require('../utils/constants');

const TowerModel = require('../models/tower');
const StationModel = require('../models/station');
const BikeModel = require('../models/bike');
const BikeUsageModel = require('../models/bike-usage');
const BikeLogModel = require('../models/bike-log');
const BadgeModel = require('../models/badge');
const AchievementModel = require('../models/achievement');

const checkAchievements = function (userId) {
    //count how many completed usages have been done
    BikeUsageModel.countDocuments({ userId: userId, to: { $ne: null } }, function (err, count) {
        if (err) { console.log("[checkAchievements] error: " + err); return; }

        //get already achieved badges
        AchievementModel.find({ userId: userId }, function (err, achieved) {
            if (err) { console.log("[checkAchievements] error: " + err); return; }

            //check which badges should be unlocked
            BadgeModel.find({ requiredRentalCount: { $lte: count } }, function (err, unlockable) {
                if (err) { console.log("[checkAchievements] error: " + err); return; }

                let toUnlock = unlockable.map(i => i._id).filter(j => !achieved.map(k => k.badgeId).includes(j._id));
                let docToInsert = toUnlock.map(i => ({ badgeId: i, userId: userId }));

                AchievementModel.insertMany(docToInsert, function (err, result) {
                    if (err) { console.log("[checkAchievements] error: " + err); }
                });
            });
        });
    });
};

exports.getTowers = function (req, res) {
    let filter = {};
    if (req.query && req.query.free) {
        filter = { bikeId: null };
    }

    TowerModel.find(filter).populate("bikeId stationId").exec(function (err, towers) {
        if (err) return res.status(500).send(err);

        let results = towers.map(modelMapper.towerToObj);
        res.status(200).send(results);
    });
};

exports.getTower = function (req, res) {
    if (!req.params.id) return res.status(400).send("Tower id not declared.");

    TowerModel.findById(req.params.id).populate("bikeId stationId").exec(function (err, tower) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.towerToObj(tower));
    });
};

exports.updateTower = function (req, res) {
    let data = {};
    if (!req.params.id) return res.status(400).send("Tower id not declared.");
    if (!req.body.name) data.name = req.body.name;

    TowerModel.findByIdAndUpdate(req.params.id, data, function (err, tower) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.towerToObj(tower));
    });
};

exports.rentBike = function (req, res) {
    if (!req.body.userId) return res.status(400).send("User id not declared.");
    if (!req.params.id) return res.status(400).send("Tower id not declared.");

    let userId = req.body.userId;

    //get tower
    TowerModel.findById(req.params.id).populate('bikeId').exec(function (err, tower) {
        if (!tower) return res.status(500).send("Unable to find the tower.");
        if (!tower.bikeId) return res.status(500).send("Tower does not have any bike.");
        if (err) return res.status(500).send(err);

        //get bike
        BikeModel.findById(tower.bikeId, function (err, bike) {
            if (err) return res.status(500).send(err);
            if (!bike) return res.status(500).send("Unable to find the tower's bike.");

            //get station
            StationModel.findById(tower.stationId, function (err, station) {
                if (err) return res.status(500).send(err);
                if (!station) return res.status(500).send("Unable to find the tower's station.");

                //update station available bikes count
                station.availableBikes--;
                station.save(function (err) {
                    if (err) return res.status(500).send(err);

                    //emit available bikes update
                    emitter.broadcastMessage(
                        constants.MessageType.StationAvailableBikesUpdate,
                        {
                            id: station._id,
                            availableBikes: station.availableBikes
                        });

                    //update tower bike reference
                    tower.bikeId = null;
                    tower.save(function(err)  {
                        if (err) return res.status(500).send(err);

                        //set bike status as busy
                        bike.currStateId = constants.BikeStates.Busy;
                        bike.save(function(err)  {
                            if (err) return res.status(500).send(err);

                            //create a new usage
                            BikeUsageModel.create({
                                bikeId: bike._id,
                                userId: userId,
                                from: tower._id,
                                to: null,
                                totalCost: null,
                                startedAt: new Date(),
                                endedAt: null
                            }, function (err) {
                                if (err) return res.status(500).send(err);

                                //create a new bike log
                                BikeLogModel.create({
                                    name: constants.BikeLogs.Rented,
                                    descr: "",
                                    userId: userId,
                                    stateId: constants.BikeStates.Busy,
                                    bikeId: bike._id,
                                    location: station.location,
                                    time: new Date()
                                }, function (err) {
                                    if (err) return res.status(500).send(err);

                                    res.status(200).send(modelMapper.bikeToObj(bike));
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};

exports.returnBike = function (req, res) {
    if (!req.params.id) return res.status(400).send("Tower id not declared.");
    if (!req.body.userId) return res.status(400).send("User id not declared.");
    if (!req.body.bikeId) return res.status(400).send("Bike id not declared.");

    let userId = req.body.userId;

    //get tower
    TowerModel.findById(req.params.id).exec(function (err, tower) {
        if (err) return res.status(500).send(err);
        if (!tower) return res.status(500).send("Unable to find the tower.");
        if (tower.bikeId) return res.status(500).send("Tower already has a bike.");

        //get bike
        BikeModel.findById(req.body.bikeId).populate('typeId').exec(function (err, bike) {
            if (err) return res.status(500).send(err);
            if (!bike) return res.status(500).send("The specified bike does not exists.");

            //get station
            StationModel.findById(tower.stationId, function (err, station) {
                if (err) return res.status(500).send(err);
                if (!station) return res.status(500).send("Unable to find the tower's station.");

                //check that the bike type is compatible with the tower
                if (!bike.typeId.equals(station.typeId)) return res.status(500).send("The specified bike cannot be returned in this tower because the type differs.");

                //update station available bikes count
                station.availableBikes++;
                station.save(function (err) {
                    if (err) return res.status(500).send(err);

                    //emit available bikes update
                    emitter.broadcastMessage(
                        constants.MessageType.StationAvailableBikesUpdate,
                        {
                            id: station._id,
                            availableBikes: station.availableBikes
                        });

                    //update tower bike reference
                    tower.bikeId = bike._id;
                    tower.save(function(err) {
                        if (err) return res.status(500).send(err);

                        //set bike status as available
                        bike.currStateId = constants.BikeStates.Available;
                        bike.save(function(err) {
                            if (err) return res.status(500).send(err);

                            //find bike usage
                            BikeUsageModel.findOne({ bikeId: bike._id, userId: userId, endedAt: null }, function (err, usage) {
                                if (err) return res.status(500).send(err);
                                if (!usage) return res.status(500).send("Unable to find usage for the specified bike and user.");

                                //compute total cost
                                let now = new Date();
                                let totalHours = Math.abs(now - usage.startedAt) / 3.6e6;
                                let totalCost = bike.typeId.hourlyRate * totalHours;

                                //update usage
                                usage.to = tower._id;
                                usage.totalCost = totalCost;
                                usage.endedAt = now;
                                usage.save(function (err) {
                                    if (err) return res.status(500).send(err);

                                    //create a new bike log
                                    BikeLogModel.create({
                                        name: constants.BikeLogs.Returned,
                                        descr: "",
                                        userId: userId,
                                        stateId: constants.BikeStates.Available,
                                        bikeId: bike._id,
                                        location: station.location,
                                        time: new Date()
                                    }, function (err) {
                                        if (err) return res.status(500).send(err);

                                        //check achievements
                                        checkAchievements(userId);

                                        res.status(200).send(modelMapper.bikeUsageToObj(usage));
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
};