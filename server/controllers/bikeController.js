const constants  = require('../utils/constants');
const emitter = require('../utils/emitter');
const mongoose = require('mongoose');
const modelMapper = require('../utils/modelMapper');

const StationModel = require('../models/station');
const TowerModel = require('../models/tower');
const BikeTypeModel = require('../models/bike-type');
const BikeLogModel = require('../models/bike-log');
const BikeUsageModel = require('../models/bike-usage');
const BikeModel = require('../models/bike');

exports.getBikes = function (req, res) {
    BikeModel.find({}).populate('typeId currStateId').exec(function (err, bikes) {
        if (err) return res.status(500).send(err);

        res.status(200).send(bikes.map(modelMapper.bikeToObj));
    });
};

exports.getBike = function (req, res) {
    if (!req.params.id) return res.status(400).send("Bike id not declared.");

    BikeModel.findById(req.params.id).populate('typeId currStateId').exec(function (err, bike) {
        if (err) return res.status(500).send(err);

        BikeLogModel.find({bikeId: bike._id}).populate("userId stateId").sort({time: -1}).exec(function(err, logs) {
            if (err) return res.status(500).send(err);

            let result = {
                bike: modelMapper.bikeToObj(bike),
                usage: {},
                station: {},
                tower: {},
                logs: logs.map(modelMapper.bikeLogToObj)
            };

            BikeUsageModel.findOne({ bikeId: bike._id, to: null, endedAt: null }).populate("userId from").exec(function(err, usage) {
                if (err) return res.status(500).send(err);

                //if the usage is defined (someone is using the bike) adds it into the result object
                if (usage) result.usage = usage;

                TowerModel.findOne({ bikeId: bike._id }).populate('stationId').exec(function (err, tower) {
                    if (err) return res.status(500).send(err);

                    //if the bike has a referenced tower, find station and fill result, else send empty objects
                    if (tower) {
                        result.tower = modelMapper.towerToObj(tower);

                        StationModel.findById(tower.stationId, function (err, station) {
                            if (err) return res.status(500).send(err);
                            if (!station) return res.status(500).send("Unable to find the station of tower.");

                            result.station = modelMapper.stationToObj(station);
                            res.status(200).send(result);
                        });
                    } else {
                        res.status(200).send(result);
                    }
                });
            });
        });
    });
};

exports.createBike = function (req, res) {
    if (!req.body.typeId) return res.status(400).send("Type id not declared.");
    if (!req.body.towerId) return res.status(400).send("Tower id not declared.");
    if (!req.body.userId) return res.status(400).send("User id not declared.");

    //gather info
    let typeId = req.body.typeId;
    let towerId = req.body.towerId;
    let userId = req.body.userId;

    //get tower
    TowerModel.findById(towerId, function (err, tower) {
        if (err) return res.status(500).send(err);
        if (tower.bikeId) return res.status(500).send("Tower is not empty.");

        //get station
        StationModel.findById(tower.stationId, function (err, station) {
            if (err) return res.status(500).send(err);

            //create bike
            BikeModel.create({
                typeId: typeId,
                currStateId: constants.BikeStates.Available,
                currLocation: station.location
            }, function (err, bike) {
                if (err) return res.status(500).send(err);

                //create bike log
                BikeLogModel.create({
                    name: constants.BikeLogs.Created,
                    descr: "Bike created",
                    userId: userId,
                    bikeId: bike._id,
                    stateId: constants.BikeStates.Available,
                    location: station.location,
                    time: new Date()
                }, function (err) {
                    if (err) return res.status(500).send(err);

                    //assign bike to tower
                    tower.bikeId = bike._id;
                    tower.save(function(err) {
                        if (err) return res.status(500).send(err);

                        //update station bikes count
                        station.availableBikes++;
                        station.save(function(err) {
                            if (err) return res.status(500).send(err);

                            //emit available bikes update
                            emitter.broadcastMessage(
                                constants.MessageType.StationAvailableBikesUpdate,
                                {
                                    id: station._id,
                                    availableBikes: station.availableBikes
                                });

                            res.status(200).send(modelMapper.bikeToObj(bike));
                        });
                    });
                });
            });
        });
    });
};

exports.updateBike = function (req, res) {
    let data = {};
    if (req.body.currStateId) data.currStateId = req.body.currStateId;
    if (req.body.currLocation) data.currLocation = req.body.currLocation;

    BikeModel.findByIdAndUpdate(req.params.id, data).populate("currStateId typeId").exec(function (err, bike) {
        if (err) return res.status(500).send(err);

        //emit bike location updates
        emitter.broadcastMessage(constants.MessageType.BikeLocationUpdate, { id: bike._id, location: bike.currLocation });

        res.status(200).send(modelMapper.bikeToObj(bike));
    });
};

exports.getTypes = function (req, res) {
    BikeTypeModel.find({}).exec(function (err, types) {
        if (err) return res.status(500).send(err);

        res.status(200).send(types.map(modelMapper.bikeTypeToObj));
    });
};

exports.startMaintenance = function (req, res) {
    if (!req.body.userId) return res.status(400).send("User id not declared.");
    if (!req.params.id) return res.status(400).send("Bike id not declared.");

    //find the target bike
    BikeModel.findById(req.params.id, function (err, bike) {
        if (err) return res.status(500).send(err);
        if (!bike) return res.status(500).send("Unable to find the bike.");
        if (bike.currStateId.equals(mongoose.Types.ObjectId(constants.BikeStates.Busy))) return res.status(412).send("The bike is currently in use.");

        //find the tower of this bike
        TowerModel.findOne({bikeId: bike._id}).populate('stationId').exec(function (err, tower) {
            if (err) return res.status(500).send(err);
            if (!tower) return res.status(500).send("Unable to find the tower for the specified bike.");
            if (tower && !tower.stationId) return res.status(500).send("Unable to find the station of tower.");

            StationModel.findById(tower.stationId, function (err, station)  {
                if (err) return res.status(500).send(err);

                //add new bike status
                BikeLogModel.create({
                    name: constants.BikeLogs.StartMaintenance,
                    descr: req.body.notes,
                    userId: req.body.userId,
                    stateId: constants.BikeStates.Maintenance,
                    bikeId: bike._id,
                    location: tower.stationId.location,
                    time: new Date()
                }, function (err) {
                    if (err) return res.status(500).send(err);

                    //set tower free
                    tower.bikeId = null;
                    tower.save(function (err) {
                        if (err) return res.status(500).send(err);

                        bike.currStateId = constants.BikeStates.Maintenance;
                        bike.save(function (err) {
                            if (err) return res.status(500).send(err);

                            station.availableBikes--;
                            station.save(function(err) {
                                if (err) return res.status(500).send(err);

                                //emit available bikes update
                                emitter.broadcastMessage(
                                    constants.MessageType.StationAvailableBikesUpdate,
                                    {
                                        id: station._id,
                                        availableBikes: station.availableBikes
                                    });

                                res.status(200).send(modelMapper.bikeToObj(bike));
                            });                           
                        });
                    });
                });
            });
        });
    });
};

exports.endMaintenance = function (req, res) {
    if (!req.params.id) return res.status(400).send("Bike id not declared.");
    if (!req.body.userId) return res.status(400).send("User id not declared.");
    if (!req.body.towerId) return res.status(400).send("Tower id not declared.");

    //find the target bike
    BikeModel.findById(req.params.id, function (err, bike) {
        if (err) return res.status(500).send(err);
        if (!bike) return res.status(500).send("Unable to find the bike.");

        //find the tower of this bike
        TowerModel.findById(req.body.towerId).populate('stationId').exec(function (err, tower) {
            if (err) return res.status(500).send(err);
            if (!tower) return res.status(500).send("Unable to find the tower.");
            if (tower && !tower.stationId) return res.status(500).send("Unable to find the station of tower.");

            StationModel.findById(tower.stationId, function (err, station)  {
                if (err) return res.status(500).send(err);

                //add new bike status
                BikeLogModel.create({
                    name: constants.BikeLogs.EndMaintenance,
                    descr: "",
                    userId: req.body.userId,
                    stateId: constants.BikeStates.Available,
                    bikeId: bike._id,
                    location: tower.stationId.location,
                    time: new Date()
                }, function (err) {
                    if (err) return res.status(500).send(err);

                    //set tower busy
                    tower.bikeId = bike._id;
                    tower.save(function (err) {
                        if (err) return res.status(500).send(err);

                        bike.currStateId = constants.BikeStates.Available;
                        bike.currLocation = tower.stationId.location;
                        bike.save(function (err) {
                            if (err) return res.status(500).send(err);

                            station.availableBikes++;
                            station.save(function(err) {
                                if (err) return res.status(500).send(err);

                                //emit available bikes update
                                emitter.broadcastMessage(
                                    constants.MessageType.StationAvailableBikesUpdate,
                                    {
                                        id: station._id,
                                        availableBikes: station.availableBikes
                                    });

                                res.status(200).send(modelMapper.bikeToObj(bike));
                            });   
                        });
                    });
                });
            });
        });
    });
};