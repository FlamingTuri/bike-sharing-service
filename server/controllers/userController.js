const bcrypt = require('bcryptjs');
const config = require('../config');
const jwt = require('jsonwebtoken');
const modelMapper = require('../utils/modelMapper');

const UserModel = require('../models/user');
const BikeUsageModel = require('../models/bike-usage');
const BadgeModel = require('../models/badge');
const AchievementModel = require('../models/achievement');

exports.login = function(req, res) {
    if (!req.body.username) return res.status(400).send("Username not declared.");
    if (!req.body.password) return res.status(400).send("Password not declared.");

    UserModel.findOne({ username: req.body.username }, function (err, user) {
        if (err) return res.status(500).send({auth: false, message: err });

        if (!user || !bcrypt.compareSync(req.body.password, user.password)) {
            return res.status(400).send({auth: false, message: "Invalid credentials" });
        }

        let token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400
        });

        res.status(200).send({ auth: true, token: token, user: modelMapper.userToObj(user) });
    });
};

exports.register = function(req, res) {
    UserModel.create({
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password),
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname
    }, function (err) {
        if (err) return res.status(500).send({ status: false, message: "Error occurred"});

        res.status(200).send({ status: true, message: "Ok" });
    });
};

exports.getUser = function (req, res) {
    if (!req.params.id) return res.status(400).send("User id not declared.");

    UserModel.findById(req.params.id, function (err, user) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.userToObj(user));
    });
};

exports.getUsers = function (req, res) {
    UserModel.find({}, function (err, users) {
        if (err) return res.status(500).send(err);

        res.status(200).send(users.map(modelMapper.userToObj));
    });
};

exports.updateUser = function (req, res) {
    if (!req.params.id) return res.status(400).send("User id not declared.");

    UserModel.findById(req.params.id, function(err, user){
        if (err) return res.send(500, err);

        let oldUser = user.toObject();
        if (req.body.password) user.password = bcrypt.hashSync(req.body.password);
        if (req.body.email) user.email = req.body.email;
        if (req.body.name) user.name = req.body.name;
        if (req.body.surname) user.surname = req.body.surname;
        if (req.body.city) user.city = req.body.city;
        if (req.body.country) user.country = req.body.country;
        if (req.body.creditCardInfo) user.creditCardInfo = req.body.creditCardInfo;

        user.save(function (err) {
            if (err) return res.send(500, err);

            res.send(modelMapper.userToObj(oldUser));
        });
    });
};

exports.getUserBike = function(req, res) {
    if (!req.params.id) return res.status(400).send("User id not declared.");

    BikeUsageModel.findOne({ userId: req.params.id, endedAt: null }, function (err, usage) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.genericToObj(usage));
    });
};

exports.getUserBadges = function (req, res) {
    if (!req.params.id) return res.status(400).send("User id not declared.");

    AchievementModel.find({ userId: req.params.id }, function (err, achievements) {
        if (err) return res.status(500).send(err);

        BadgeModel.find({}, function (err, badges) {
            if (err) return res.status(500).send(err);

            let result = badges.map(i => {
                return {
                    name: i.name,
                    descr: i.descr,
                    color: i.color,
                    achieved: (achievements.filter(a => a.badgeId.equals(i._id)).length >= i.requiredRentalCount)
                };
            });

            res.status(200).send(result);
        });
    });
};