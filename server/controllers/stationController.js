const modelMapper = require('../utils/modelMapper');

const BikeTypeModel = require('../models/bike-type');
const StationModel = require('../models/station');
const TowerModel = require('../models/tower');

exports.getStations = function (req, res) {
    let filter = {};
    if (req.query && req.query.type) {
        filter = { typeId: req.query.type };
    }

    StationModel.find(filter).populate('typeId').exec(function (err, stations) {
        if (err) return res.status(500).send(err);

        res.status(200).send(stations.map(modelMapper.stationToObj));
    });
};

exports.getGroupedStationsAndTowersByType = function (req, res) {
    StationModel.aggregate([
        {
            //estrazione torrette
            $lookup: {
                from: 'towers',
                localField: '_id',
                foreignField: 'stationId',
                as: 'towers_lookup'
            }
        },
        {
            //espansione delle torrette
            $unwind: {
                path: '$towers_lookup'
            }
        },
        {
            //estrazione tipo bici
            $lookup: {
                from: 'bike-types',
                localField: 'typeId',
                foreignField: '_id',
                as: 'biketype_lookup'
            }
        },
        {
            //espansione dei tipi delle bici
            $unwind: {
                path: '$biketype_lookup'
            }
        },
        {
            //filtra solo per quelle libere
            $match: { 'towers_lookup.bikeId': null  }
        },
        {
            $group: {
                //la chiave su cui fare il group
                _id: "$typeId",
                type: {
                    //prende solo la prima occorrenza
                    $first: {
                        id: '$biketype_lookup._id',
                        name: '$biketype_lookup.name',
                        descr: '$biketype_lookup.descr'
                    }
                },
                stations: {
                    //set senza duplicati
                    $addToSet: {
                        id: '$_id',
                        name: '$name'
                    }
                },
                towers: {
                    //push per tutti gli elementi
                    $push: {
                        id: '$towers_lookup._id',
                        name: '$towers_lookup.name',
                        stationId: '$towers_lookup.stationId'
                    }
                }
            }
        }
    ]).exec(function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(result);
    });
};

exports.getStation = function (req, res) {
    if (!req.params.id) return res.status(400).send("Station id not declared.");

    StationModel.findById(req.params.id).populate('typeId').exec(function (err, station) {
        if (err) return res.status(500).send(err);

        TowerModel.find({ stationId: station._id }, function (err, towers) {
            if (err) return res.status(500).send(err);

            let result = {
                station: modelMapper.stationToObj(station),
                towers: towers.map(modelMapper.towerToObj)
            }

            res.status(200).send(result);
        });        
    });
};

exports.createStation = function (req, res) {

    BikeTypeModel.findById(req.body.typeId, function (err, type) {
        if (err) return res.status(500).send(err);

        StationModel.create({
            name: req.body.name,
            typeId: req.body.typeId,
            location: {
                lat: req.body.location.lat,
                long: req.body.location.long,
            },
            towersCount: req.body.towersCount
        }, function (err, station) {
            if (err) return res.status(500).send(err);

            //create n empty towers
            let newTowers = [];
            Array.from(Array(station.towersCount).keys()).forEach((i) => {
                newTowers.push({
                    name: "Tower #" + i,
                    bikeId: null,
                    stationId: station._id
                });
            });

            TowerModel.insertMany(newTowers, function (err, towers) {
                if (err) return res.status(500).send(err);

                res.status(200).send({
                    station: modelMapper.stationToObj(station),
                    towers: towers.map(modelMapper.towerToObj)
                });
            });
        });
    });
};

exports.updateStation = function (req, res) {
    let data = {};
    if (!req.params.id) return res.status(400).send("Station id not declared.");
    if (!req.body.name) data.name = req.body.name;

    StationModel.findByIdAndUpdate(req.params.id, req.body, function (err, station) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.stationToObj(station));
    });
};