const modelMapper = require('../utils/modelMapper');

const BikeUsageModel = require('../models/bike-usage');

exports.getTotalRentalCount = function (req, res) {
    BikeUsageModel.aggregate([{
        $group: {
            _id: null,
            averageCost: { $avg: "$totalCost" },
            count: { $sum: 1 }
        }
    }], function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(modelMapper.groupedToObj(result[0]));
    });
};

exports.getTotalRentalCountByType = function (req, res) {
    BikeUsageModel.aggregate([
        {
            $unwind: "$bikeId"
        },
        {
            $lookup: {
                from: 'bikes',
                localField: 'bikeId',
                foreignField: '_id',
                as: 'bike_info'
            }
        },
        {
            "$unwind": "$bike_info"
        },
        {
            $lookup: {
                from: 'bike-types',
                localField: 'bike_info.typeId',
                foreignField: '_id',
                as: 'bike_type_info'
            }
        },
        {
            "$unwind": "$bike_type_info"
        },
        {
            $group: {
                _id: '$bike_type_info._id',
                type: {
                    $first: '$bike_type_info.name'
                },
                averageCost: { $avg: "$totalCost" },
                count: { $sum: 1 }
            }
        }
    ], function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(result.map(modelMapper.groupedToObj));
    });
};

exports.getAverageRentalTimeByType = function (req, res) {
    BikeUsageModel.aggregate([
        {
            $unwind: "$bikeId"
        },
        {
            $lookup: {
                from: 'bikes',
                localField: 'bikeId',
                foreignField: '_id',
                as: 'bike_info'
            }
        },
        {
            "$unwind": "$bike_info"
        },
        {
            $lookup: {
                from: 'bike-types',
                localField: 'bike_info.typeId',
                foreignField: '_id',
                as: 'bike_type_info'
            }
        },
        {
            "$unwind": "$bike_type_info"
        },
        {
            $group: {
                _id: '$bike_type_info._id',
                type: {
                    $first: '$bike_type_info.name'
                },
                averageTime: {
                    $avg: {
                        $subtract: [
                            { $ifNull: [ "$endedAt", "$startedAt" ] },
                            "$startedAt"
                        ]
                    },
                },
                count: { $sum: 1 }
            }
        }
    ], function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(result.map(modelMapper.groupedToObj));
    });
};

exports.getStationsUsageCount = function (req, res) {
    BikeUsageModel.aggregate([
        {
            $unwind: {
                path: "$from",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'towers',
                localField: 'from',
                foreignField: '_id',
                as: 'tower_from_info'
            }
        },
        {
            $unwind: {
                path: "$tower_from_info",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'stations',
                localField: 'tower_from_info.stationId',
                foreignField: '_id',
                as: 'station_from_info'
            }
        },
        {
            $unwind: {
                path: "$to",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'towers',
                localField: 'to',
                foreignField: '_id',
                as: 'tower_to_info'
            }
        },
        {
            $unwind: {
                path: "$tower_to_info",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'stations',
                localField: 'tower_to_info.stationId',
                foreignField: '_id',
                as: 'station_to_info'
            }
        },
        {
            $project: {
                station: {
                    $concatArrays: [
                        { $ifNull: [ "$station_from_info", undefined ] },
                        { $ifNull: [ "$station_to_info", undefined ] }
                    ]
                },
                totalCost: 1,
                startedAt: 1,
                endedAt: 1
            }
        },
        {
            $unwind: "$station"
        },
        {
            $group: {
                _id: '$station._id',
                name: { $first: '$station.name' },
                count: { $sum: 1 }
            }
        },
        {
            $sort : { count : -1 }
        }
    ], function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(result.map(modelMapper.groupedToObj));
    });
};

exports.getMonthlyBikeRentalCount = function (req, res) {
    BikeUsageModel.aggregate([
        {
            $unwind: "$bikeId"
        },
        {
            $lookup: {
                from: 'bikes',
                localField: 'bikeId',
                foreignField: '_id',
                as: 'bike_info'
            }
        },
        {
            $unwind: "$bike_info"
        },
        {
            $lookup: {
                from: 'bike-types',
                localField: 'bike_info.typeId',
                foreignField: '_id',
                as: 'bike_type_info'
            }
        },
        {
            $unwind: "$bike_type_info"
        },
        {
            $group: {
                _id: {
                    id: '$bike_type_info._id',
                    month: {
                        $month: "$startedAt"
                    },
                    bike_type_info: '$bike_type_info'
                },
                count: { $sum: 1 }
            }
        },
        {
            $group: {
                _id : "$_id.month",
                monthlyUsage: {
                    $push: {
                        typeId: '$_id.bike_type_info._id',
                        typeName: '$_id.bike_type_info.name',
                        count: "$count"
                    }
                }
            }
        },
        {
            $sort : { _id : 1 }
        }
    ], function (err, result) {
        if (err) return res.status(500).send(err);

        res.status(200).send(result);
    });
};


