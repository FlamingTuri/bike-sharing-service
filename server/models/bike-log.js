const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'bike-logs';

const BikeModel = require('./bike');
const UserModel = require('./user');
const BikeState = require('./bike-state');

// create a schema
const bikeLogSchema = new Schema({
    name: { type: String, required: true },
    descr: String,
    userId: { type: mongoose.Types.ObjectId, ref: UserModel.documentName, required: true },
    bikeId: { type: mongoose.Types.ObjectId, ref: BikeModel.documentName, required: true },
    stateId: { type: mongoose.Types.ObjectId, ref: BikeState.documentName, required: true },
    location: {
        lat: { type: Number, required: true },
        long: { type: Number, required: true },
    },
    time: { type: Date, required: true }
});

module.exports = mongoose.model(documentName, bikeLogSchema);
module.exports.documentName = documentName;