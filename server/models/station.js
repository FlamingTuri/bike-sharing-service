const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'stations';

const BikeTypeModel = require('./bike-type');

//create a schema
const stationSchema = new Schema({
    name: { type: String, required: true },
    typeId: { type: mongoose.Types.ObjectId, ref: BikeTypeModel.documentName, required: true },
    location: {
        lat: { type: Number, required: true },
        long: { type: Number, required: true },
    },
    availableBikes: { type: Number, default: 0 },
    towersCount: { type: Number, default: 0 },
    createdAt: Date,
    updatedAt: Date
});

// on every save, add the date
stationSchema.pre('save', function(next) {
    let currentDate = new Date();
    this.updatedAt = currentDate;
    if (!this.createdAt) this.createdAt = currentDate;
    next();
});

module.exports = mongoose.model(documentName, stationSchema);
module.exports.documentName = documentName;