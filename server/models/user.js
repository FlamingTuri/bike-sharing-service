const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'users';

// create a schema
const userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    name: String,
    surname: String,
    city: String,
    country: String,
    creditCardInfo: {
        name: String,
        cardNumber: Number,
        expirationDate: Date,
        cvCode: Number
    },
    admin: { type: String, default: false },
    createdAt: Date,
    updatedAt: Date
});

// on every save, add the date
userSchema.pre('save', function(next) {
  let currentDate = new Date();
  this.updatedAt = currentDate;
  if (!this.createdAt) this.createdAt = currentDate;
  next();
});

module.exports = mongoose.model(documentName, userSchema);
module.exports.documentName = documentName;