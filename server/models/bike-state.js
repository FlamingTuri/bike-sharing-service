const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'bike-states';

// create a schema
const bikeStatusSchema = new Schema({
    name: { type: String, required: true },
    descr: String
});

module.exports = mongoose.model(documentName, bikeStatusSchema);
module.exports.documentName = documentName;