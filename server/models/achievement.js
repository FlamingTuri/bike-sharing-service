const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'achievements';

const BadgeModel = require('./badge');
const UserModel = require('./user');

// create a schema
const achievementSchema = new Schema({
    badgeId: { type: mongoose.Types.ObjectId, ref: BadgeModel.documentName, required: true },
    userId: { type: mongoose.Types.ObjectId, ref: UserModel.documentName, required: true }
});

module.exports = mongoose.model(documentName, achievementSchema);
module.exports.documentName = documentName;