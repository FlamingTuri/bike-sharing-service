const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'bike-usages';

const UserModel = require('./user');
const TowerModel = require('./tower');
const BikeModel = require('./bike');

// create a schema
const bikeUsageSchema = new Schema({
    bikeId: { type: mongoose.Types.ObjectId, ref: BikeModel.documentName, required: true },
    userId: { type: mongoose.Types.ObjectId, ref: UserModel.documentName, required: true },
    from: { type: mongoose.Types.ObjectId, ref: TowerModel.documentName, required: true },
    to: { type: mongoose.Types.ObjectId, ref: TowerModel.documentName },
    totalCost: Number,
    startedAt: { type: Date, required: true },
    endedAt: Date
});

module.exports = mongoose.model(documentName, bikeUsageSchema);
module.exports.documentName = documentName;