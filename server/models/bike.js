const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'bikes';
const BikeTypeModel = require('./bike-type');
const BikeStateModel = require('./bike-state');

// create a schema
const bikeSchema = new Schema({
    typeId: { type: mongoose.Types.ObjectId, ref: BikeTypeModel.documentName, required: true },
    currStateId: { type: mongoose.Types.ObjectId, ref: BikeStateModel.documentName, required: true },
    currLocation: {
        lat: Number,
        long: Number
    },
    createdAt: Date,
    updatedAt: Date
});

// on every save, add the date
bikeSchema.pre('save', function(next) {
  let currentDate = new Date();
  this.updatedAt = currentDate;
  if (!this.createdAt) this.createdAt = currentDate;
  next();
});

module.exports = mongoose.model(documentName, bikeSchema);
module.exports.documentName = documentName;