const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'towers';

const BikeModel = require('./bike');
const StationModel = require('./station');

//create a schema
const towerSchema = new Schema({
    name: { type: String, required: true },
    bikeId: { type: mongoose.Types.ObjectId, ref: BikeModel.documentName },
    stationId: { type: mongoose.Types.ObjectId, ref: StationModel.documentName, required: true },
    createdAt: Date,
    updatedAt: Date
});

// on every save, add the date
towerSchema.pre('save', function(next) {
    let currentDate = new Date();
    this.updatedAt = currentDate;
    if (!this.createdAt) this.createdAt = currentDate;
    next();
});

module.exports = mongoose.model(documentName, towerSchema);
module.exports.documentName = documentName;