const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'bike-types';

// create a schema
const bikeTypeSchema = new Schema({
    name: { type: String, required: true },
    descr: String,
    hourlyRate: { type: Number, required: true }
});

module.exports = mongoose.model(documentName, bikeTypeSchema);
module.exports.documentName = documentName;