const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentName = 'badges';

// create a schema
const badgeSchema = new Schema({
    name: { type: String, required: true },
    descr: { type: String, required: true },
    color: { type: String, required: true },
    requiredRentalCount: { type: Number, required: true }
});

module.exports = mongoose.model(documentName, badgeSchema);
module.exports.documentName = documentName;