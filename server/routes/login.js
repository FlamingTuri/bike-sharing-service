const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

// Executes login
router.post('/', userController.login);

module.exports = router;

