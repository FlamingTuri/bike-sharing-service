const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

// Executes registration
router.post('/', userController.register);

module.exports = router;

