const express = require('express');
const router = express.Router();
const statsController = require('../controllers/statsController');

router.get('/total-rent-count', statsController.getTotalRentalCount);

router.get('/total-rent-count-by-type', statsController.getTotalRentalCountByType);

router.get('/average-rent-time-by-type', statsController.getAverageRentalTimeByType);

router.get('/stations-usages-count', statsController.getStationsUsageCount);

router.get('/monthly-rent-count-by-type', statsController.getMonthlyBikeRentalCount);

module.exports = router;