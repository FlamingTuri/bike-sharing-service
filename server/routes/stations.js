const express = require('express');
const router = express.Router();
const stationController = require('../controllers/stationController');

// Gets all stations
router.get('/', stationController.getStations);

//Gets a grouped collections of stations and towers by bike type
router.get('/grouped', stationController.getGroupedStationsAndTowersByType);

// Gets a station with specific id
router.get('/:id', stationController.getStation);

// Creates a station
router.post('/', stationController.createStation);

// Updates a station
router.put('/:id', stationController.updateStation);

module.exports = router;