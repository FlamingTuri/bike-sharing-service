const express = require('express');
const router = express.Router();
const emitter = require('../utils/emitter');

// Establish a websocket
router.ws('/', function(ws, req) {
    ws.on('close', function () {
        console.log("ws closed.");
        emitter.purge();
    });

    emitter.attach(ws);
});

module.exports = router;