const express = require('express');
const router = express.Router();
const towerController = require('../controllers/towerController');

// Gets all towers
router.get('/', towerController.getTowers);

// Gets a tower
router.get('/:id', towerController.getTower);

// Updates a tower
router.put('/:id', towerController.updateTower);

// Lets a user to rent the bike in the tower
router.post('/:id/rent-bike', towerController.rentBike);

// Lets a user to return the bike in the tower
router.post('/:id/return-bike', towerController.returnBike);

module.exports = router;