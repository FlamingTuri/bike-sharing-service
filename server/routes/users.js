const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

// Gets all users
router.get('/', userController.getUsers);

// Gets a user with specific id
router.get('/:id', userController.getUser);

// Updates a user with specific id
router.put('/:id', userController.updateUser);

// Gets last user rented bike
router.get('/:id/bike', userController.getUserBike);

// Gets all users badges
router.get('/:id/badges', userController.getUserBadges);

module.exports = router;