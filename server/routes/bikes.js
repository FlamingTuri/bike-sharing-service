const express = require('express');
const router = express.Router();
const bikeController = require('../controllers/bikeController');

// Get all bikes
router.get('/', bikeController.getBikes);

// Creates a bike
router.post('/', bikeController.createBike);

// Get all bike types
router.get('/types', bikeController.getTypes);

// Get a bike with specific id
router.get('/:id', bikeController.getBike);

// Update a bike with specific id
router.put('/:id', bikeController.updateBike);

// Starts the maintenance on a specific bike
router.post('/:id/start-maintenance', bikeController.startMaintenance);

// Finish the maintenance on a specific bike
router.post('/:id/end-maintenance', bikeController.endMaintenance);

module.exports = router;