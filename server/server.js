const config = require('./config');
const https = require('https');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jwt = require('express-jwt');
require('./database/db');

//config server
const app = express();
//create https server
const server = https.createServer({
    key: fs.readFileSync("./certificate/key.pem"),
    cert: fs.readFileSync("./certificate/cert.pem")
}, app);
//attach websocket capabilities to express
require('express-ws')(app, server);

//allow cross-reference
app.use(require('cors')());
//attach logger
app.use(morgan('combined'));
//body parsing url encoded or json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//json web token middleware
app.use(jwt({ secret: config.secret }).unless({
    path: [
        /\/app/i,
        '/api/register',
        '/api/login',
        '/api/subscribe/.websocket'
    ]
}));
//redirect from root to the client virtual path if the requested url is not composed
app.use(function(err, req, res, next) {
    let valid = /^\/[^\/]*$/gm.test(req.originalUrl);
    if (err.status === 401 && valid) {
        res.redirect(config.client.virtualPath + req.originalUrl);
    } else {
        next();
    }
});

//statically serve client page
app.use(config.client.virtualPath, express.static(config.client.physicalPath));

//rest api routes
app.use('/api/login', require('./routes/login'));
app.use('/api/register', require('./routes/register'));
app.use('/api/users', require('./routes/users'));
app.use('/api/bikes', require('./routes/bikes'));
app.use('/api/stations', require('./routes/stations'));
app.use('/api/towers', require('./routes/towers'));
app.use('/api/stats', require('./routes/stats'));
app.use('/api/subscribe', require('./routes/subscribe'));

server.listen(config.server.port, function () {
    console.log('Server started listening on: https://' + config.server.address + ':' + config.server.port);
});