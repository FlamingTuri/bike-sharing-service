require('../database/db');
const constants = require('../utils/constants');
const BikeModel = require('../models/bike');
const request = require('request');

const baseUrl = 'https://localhost:8443/';
const loginUrl = 'api/login';
const bikeUrl = 'api/bikes/';
const username = 'dummy';
const password = 'dummy';
const interval = 1500;
const delta = -0.01;
let token = '';

//dangerous, but who cares
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

//Get jwt token
request.post(baseUrl + loginUrl, {
        json: {
            username: username,
            password: password
        }
    },
    function (error, response, body) {
        if (error) {
            console.log('Unable to login: ' + error);
            process.exit(1);
        } else {
            console.log("Successfully logged.");
            token = body.token;
        }
    });

//Send location update for a bike
function sendUpdate(bike) {

    let newLoc = {
        lat: (bike.currLocation.lat + delta).toFixed(5),
        long: (bike.currLocation.long + delta).toFixed(5)
    };

    request.put(
        baseUrl + bikeUrl + bike._id,
        {
            json: {
                currLocation: newLoc
            },
            auth: {
                'bearer': token
            }
        },
        function (error) {
            if (error) {
                console.log('Unable to update position: ' + error)
            } else {
                console.log('Location updated [bikeId: ' + bike._id.toString() + '].');
            }
        }
    );
}

//do stuff repeatedly
let intervalToken = setInterval(function() {
    console.log("Updating bikes locations...");
    BikeModel.find({ currStateId: { $ne: constants.BikeStates.Maintenance } }, function (err, bikes) {
        let max = bikes.length < 5 ? bikes.length : 5;
        for (let i = 0; i < max; i++) {
            const rnd = Math.floor(Math.random() * bikes.length);
            console.log('Updating bike location [bikeId: ' + bikes[rnd]._id.toString() + ']...');
            sendUpdate(bikes[rnd]);
        }
    });
}, interval);

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

rl.on('line', function(line){
    if (line === "kill") {
        console.log("killing...");
        clearInterval(intervalToken);
        setTimeout(() => { process.exit(-1); }, 2000);
    } else {
        console.log("I don't understand: " + line);
    }
});

