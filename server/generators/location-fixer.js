require('../database/db');
const BikeModel = require('../models/bike');
const StationModel = require('../models/station');
const BikeLogModel = require('../models/bike-log');

BikeModel.find({}, function (err, bikes) {
    for (let i = 0; i < bikes.length; i++) {
        let e = bikes[i];
        e.currLocation = {
            lat: e.currLocation.lat.toFixed(5),
            long: e.currLocation.long.toFixed(5)
        };
        e.save(function (err) {
            if (err) console.log("Bike update failed! " + err.toString());
        });
    }
});

StationModel.find({}, function (err, stations) {
    for (let i = 0; i < stations.length; i++) {
        let e = stations[i];
        e.location = {
            lat: e.location.lat.toFixed(5),
            long: e.location.long.toFixed(5)
        };
        e.save(function (err) {
            if (err) console.log("Station update failed! " + err.toString());
        });
    }
});

BikeLogModel.find({}, function (err, logs) {
    for (let i = 0; i < logs.length; i++) {
        let e = logs[i];
        e.location = {
            lat: e.location.lat.toFixed(5),
            long: e.location.long.toFixed(5)
        };
        e.save(function (err) {
            if (err) console.log("Bike Log update failed! " + err.toString());
        });
    }
});

