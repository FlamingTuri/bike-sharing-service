require('../database/db');
const TowerModel = require('../models/tower');
const BikeUsageModel = require('../models/bike-usage');
const request = require('request');

const baseUrl = 'https://localhost:8443/';
const loginUrl = 'api/login';
const towersUrl = 'api/towers/';
const returnUrl = '/return-bike';
const rentUrl = '/rent-bike';

const interval = 15000;
const username = 'dummy';
const password = 'dummy';
let token = '';
let userId = '';
let randomizeMonth = false;

//dangerous, but who cares
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

//Get jwt token
request.post(baseUrl + loginUrl, {
        json: {
            username: username,
            password: password
        }
    },
    function (error, response, body) {
        if (error) {
            console.log('Unable to login: ' + error);
            process.exit(1);
        } else {
            console.log("Successfully logged.");
            token = body.token;
            userId = body.user.id;
        }
    });

function requestRental(rentTower, returnTower) {  
    request.post(baseUrl + towersUrl + rentTower._id + rentUrl, {
        json: {
            userId: userId
        },
        auth: {
            'bearer': token
        }
    },
    function (err, response, body) {
        if (err) {
            console.log('Unable to rent bike: ' + err);
        } else {
            console.log('Rent completed [towerId: ' + rentTower._id.toString() + ', userId: ' + userId + '], requesting return...');
            requestReturn(returnTower, body);
        }
    })   
}

function requestReturn(tower, bike) {
    setTimeout(function () {
        request.post(baseUrl + towersUrl + tower._id + returnUrl,
            {
                json: {
                    userId: userId,
                    bikeId: bike.id
                },
                auth: {
                    'bearer': token
                }
            }, function (err, response, body) {
                if (err) {
                    console.log('Unable to return bike: ' + err);
                } else if (!body.startedAt) {
                    console.log('Unable to return bike: ' + body);
                } else {                   
                    
                    if (randomizeMonth) {
                        let rnd = Math.floor(Math.random() * 12);
                        let newStartDate = new Date(body.startedAt);
                        newStartDate.setMonth(rnd);
    
                        BikeUsageModel.findOneAndUpdate({ _id: body.id }, { startedAt: newStartDate }, { "new": true }, function (err, usage) {
                            if (err) {
                                console.log('Unable to update usage: ' + err);
                            } else {
                                console.log('Return completed [towerId: ' + tower._id.toString() + ', bikeId: ' + bike.id + ', userId: ' + userId + '].');
                            }
                        });
                    } else {
                        console.log('Return completed [towerId: ' + tower._id.toString() + ', bikeId: ' + bike.id + ', userId: ' + userId + '].');
                    }            
                }
            }
        )
    }, 1000 * Math.floor(Math.random() * 10));
}

//do stuff repeatedly
const intervalToken = setInterval(function() {
    console.log('Generating bike usages...');
    TowerModel.find({}).populate('stationId bikeId').exec(function (err, towers) {
        const towersWithBikes = towers.filter(i => i.bikeId != null);
        let from = towersWithBikes[Math.floor(Math.random() * towersWithBikes.length)];
        const towersWithoutBikes = towers.filter(i => i.bikeId == null && from.bikeId.typeId.equals(i.stationId.typeId));
        let to = towersWithoutBikes[Math.floor(Math.random() * towersWithoutBikes.length)];
        requestRental(from, to);
    });
}, interval);

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

rl.on('line', function(line){
    if (line === "kill") {
        console.log("killing...");
        clearInterval(intervalToken);
        setTimeout(() => { process.exit(-1); }, 30000);
    } else if (line === "randMonth") {
        randomizeMonth = !randomizeMonth;
        console.log("switching mode: " + (randomizeMonth ? "randMonth" : "notRandMonth"));      
    } else {
        console.log("I don't understand: " + line);
    }
});