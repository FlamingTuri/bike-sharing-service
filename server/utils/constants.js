module.exports = {
    BikeLogs: { //bike logs possible names
        Created: "Created",
        StartMaintenance: "Maintenance started",
        EndMaintenance: "Maintenance ended",
        Rented: "Rented",
        Returned: "Returned"
    },
    BikeStates: { //object ids
        Available: "5bedd8f8e7179a56e2114090",
        Busy: "5bedd911e7179a56e211409b",
        Maintenance: "5bedd93fe7179a56e21140b1"
    },
    BikeTypes: { //object ids
        Normal: "5be9eb76e7179a27a535e811",
        Electric: "5be9eba4e7179a27a535e88f"
    },
    MessageType: {
        BikeLocationUpdate: 0,
        StationAvailableBikesUpdate: 1
    }
};