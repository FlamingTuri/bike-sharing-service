let activeWs = [];

exports.broadcastMessage = function(type, message) {
    let json = {
        type: type,
        data: message
    };
    activeWs.forEach(ws => ws.send(JSON.stringify(json)));
};

exports.attach = function(ws) {
    activeWs.push(ws);
};

exports.purge = function () {
    activeWs = activeWs.filter(i => i.isAlive);
};