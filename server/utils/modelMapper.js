const mongoose = require('mongoose');

//Checks if the input is a simple object or a document. If it's a document transforms it to json object,
const mapToObj = function (input) {
    return  (!input) ? {} : ((input.toObject) ? input.toObject() : input);
};

const genericToObj = function (input) {
    //if it's an object id (non populated, just return the id), otherwise it will loop indefinitely
    if (input instanceof mongoose.Types.ObjectId) { return input.toString(); }

    let data = mapToObj(input);
    data.id = data._id;
    delete data._id;
    delete data.__v;
    return data;
};

const groupedToObj = function (input) {
    //if it's an object id (non populated, just return the id), otherwise it will loop indefinitely
    if (input instanceof mongoose.Types.ObjectId) { return input.toString(); }

    let data = mapToObj(input);
    delete data._id;
    return data;
};

const bikeToObj = function(bike) {
    let data = mapToObj(bike);
    data.type = genericToObj(data.typeId);
    delete data.typeId;
    data.currState = genericToObj(data.currStateId);
    delete data.currStateId;
    return genericToObj(data);
};

const bikeLogToObj = function(bikeLog) {
    let data = mapToObj(bikeLog);
    data.user = userToObj(data.userId);
    delete data.userId;
    data.bike = genericToObj(data.bikeId);
    delete data.bikeId;
    data.state = genericToObj(data.stateId);
    delete data.stateId;
    return genericToObj(data);
};

const bikeUsageToObj = function (usage) {
    let data = mapToObj(usage);
    data.bike = genericToObj(data.bikeId);
    delete data.bikeId;
    data.user = userToObj(data.userId);
    delete data.userId;
    data.from = towerToObj(data.from);
    if (data.to) {
        data.to = towerToObj(data.to);
    } else {
        data.to = null;
    }
    return genericToObj(data);
};

const badgeToObj = function (badge) {
    let data = mapToObj(badge);
    data.user = userToObj(data.userId);
    delete data.userId;
    return genericToObj(data);
};

const stationToObj = function (station) {
    let data = mapToObj(station);
    data.type = genericToObj(data.typeId);
    delete data.typeId;
    return genericToObj(data);
};

const towerToObj = function (tower) {
    let data = mapToObj(tower);
    if (data.bikeId) {
        data.bike = bikeToObj(data.bikeId);
    } else {
        data.bike = null;
    }
    delete data.bikeId;
    data.station = stationToObj(data.stationId);
    delete data.stationId;
    return genericToObj(data);
};

const userToObj = function(user) {
    let data = mapToObj(user);
    delete data.password;
    //delete data.createdAt;
    //delete data.updatedAt;
    return genericToObj(data);
};

module.exports.genericToObj = genericToObj;
module.exports.bikeStateToObj = genericToObj;
module.exports.bikeTypeToObj = genericToObj;
module.exports.bikeToObj = bikeToObj;
module.exports.bikeLogToObj = bikeLogToObj;
module.exports.bikeUsageToObj = bikeUsageToObj;
module.exports.badgeToObj = badgeToObj;
module.exports.stationToObj = stationToObj;
module.exports.towerToObj = towerToObj;
module.exports.userToObj = userToObj;
module.exports.groupedToObj = groupedToObj;