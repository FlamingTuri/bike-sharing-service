#!/bin/bash

PORT=4200
ip=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *//p;q}')

if [[ $# -ge 1 ]] ; then
  re='^[0-9]+$'
  if [[ $1 =~ $re ]] ; then
    PORT=$1
  else
    echo "error: arg '$1' is not a number" >&2; exit 1
  fi
fi

cd "$(dirname "$0")"
cd '../client'

SEPARATOR="\n----------------------------------------------\n"
echo -e "$SEPARATOR"
echo "  machine current ip: $ip "
echo -e "$SEPARATOR"

# -o opens the application in a new tab of your default browser
# --port $PORT specifies the running port of the application
# --host 0.0.0.0 allows devices on the same network to run 
#   the application by opening a browser and going to $ip:$PORT

CERTPATH='../server/certificate/cert.pem'
KEYPATH='../server/certificate/key.pem'
ng serve -o --port $PORT --host 0.0.0.0 --ssl true --ssl-cert $CERTPATH --ssl-key $KEYPATH
