#!/bin/bash

# cd to the current script directoy
cd "$(dirname "$0")"

# start the server in a new terminal
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux
        if [ -f $(which gnome-terminal) ]
        then
            # exec bash prevents the terminal window close after the running script terminantes 
            gnome-terminal -- bash -c './start-server.sh; exec bash'
        else
            x-terminal-emulator -e './start-server.sh; exec bash'
        fi
        ;;
    Darwin*)    machine=Mac
        open -a Terminal start-server.sh
        # not working, the new opened terminal needs to cd to the script directory
        # osascript -e 'tell application "Terminal" to do script "cd path; ./start-server.sh"'
        ;;
    #CYGWIN*)    machine=Cygwin;;
    #MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

# start the client
./start-client.sh
