cd /D "%~dp0"
cd ..\client

set CERTPATH=" ..\server\certificate\cert.pem"
set KEYPATH="..\server\certificate\key.pem"
ng serve -o --host 0.0.0.0 --ssl true --ssl-cert %CERTPATH% --ssl-key %KEYPATH%
