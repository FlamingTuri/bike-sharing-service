import { Component, OnInit, Input } from '@angular/core';
import { NavItem } from '../model/nav-item';
import { ResizeService } from '../utils/resize.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Input() sideNavContent: NavItem[];

  currentPageDisplayed: string = "";
  sidebarOpened = false;

  resizeSubscription;
  isTablet: boolean;

  constructor(
    public route: Router,
    private resizeService: ResizeService
  ) { }


  ngOnInit() {
    if (this.includes("bikeDetails") || this.includes("stationDetails") || this.includes("addStations")) {
      this.currentPageDisplayed = "Manage";
    } else if (this.includes("userDetails")) {
      this.currentPageDisplayed = "User";
    } else {
      try {
        this.currentPageDisplayed = this.sideNavContent.find(e => e.routerLink == this.route.url).nameDisplayed;
      } catch (Error) {
        this.currentPageDisplayed = "";
      }
    }


    this.isTablet = this.resizeService.isTablet;
    this.sidebarOpened = !this.resizeService.isTablet;

    this.resizeSubscription = this.resizeService.onResize$.subscribe(size => {
      this.isTablet = this.resizeService.isTablet;
      this.sidebarOpened = !this.resizeService.isTablet;
    });

  }

  includes(a: string): boolean {
    return this.route.url.includes(a);
  }

  changeCurrentPageDisplayed(nav) {
    this.currentPageDisplayed = nav.nameDisplayed;
    if (this.isTablet) {
      this.sidebarOpened = false;
    }
  }

  toggle() {
    this.sidebarOpened = !this.sidebarOpened;
  }
}
