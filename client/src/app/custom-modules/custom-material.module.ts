import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatSidenavModule, MatButtonModule, MatCheckboxModule,
  MatIconModule, MatToolbarModule, MatListModule, MatTabsModule,
  MatFormFieldModule, MatInputModule, MatDialogModule, MatOptionModule,
  MatSelectModule, MatRadioModule, MatExpansionModule, MatNativeDateModule, MatPaginatorModule, MatProgressSpinnerModule
} from '@angular/material';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    MatExpansionModule,
    MatCardModule,
    MatDatepickerModule,
    MatGridListModule,
    MatTooltipModule,
    MatTableModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule
  ],
  exports: [
    MatSidenavModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    MatExpansionModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTooltipModule,
    MatTableModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule
  ],
  declarations: []
})
export class CustomMaterialModule { }
