import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-floating-button',
  templateUrl: './floating-button.component.html',
  styleUrls: ['./floating-button.component.css']
})
export class FloatingButtonComponent implements OnInit {

  @Input('icon') icon: String;
  @Input('text') text: String;
  @Output() press = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onPress() {
    this.press.emit()
  }
}
