import {Component, Input, OnInit} from '@angular/core';
import {User} from "../user";

@Component({
  selector: 'app-user-row',
  templateUrl: './user-row.component.html',
  styleUrls: ['./user-row.component.css']
})
export class UserRowComponent implements OnInit {

  @Input() user: User;
  isAdmin: String = "No";

  constructor() { }

  ngOnInit() {
    if(this.user.admin == "true") {
      this.isAdmin = "Yes"
    }
  }

}
