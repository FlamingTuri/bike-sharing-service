import { Component, OnInit } from '@angular/core';
import {ResizeService} from "../utils/resize.service";
import {User} from "./user";
import {UserService} from "./user.service";

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  users$: User[] = [];
  typeSelected: String = 'all';
  initCode: String ="";
  displayMode: String = "33.3%";

  constructor(private  resizer: ResizeService, private data: UserService) {
    resizer.onResize$.subscribe(() => window.innerWidth < 768 ? this.displayBlock() : this.displayFlex());
    if (this.resizer.isMobile) {
      this.displayBlock()
    }
  }

  ngOnInit() {
    this.data.getUsers().then(res => this.users$ = res)
                        .catch(err => console.log("GET user list: " + err))
  }

  displayBlock() {
    this.displayMode = "100%"
  }

  displayFlex() {
    this.displayMode = "33.3%"
  }

}
