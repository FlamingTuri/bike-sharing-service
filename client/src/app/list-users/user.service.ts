import { Injectable } from '@angular/core';
import {HttpOperationsService} from "../utils/http-operations.service";
import {RestApiService} from "../utils/rest-api.service";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpOperationsService, private restApiService: RestApiService) { }

  getUsers(): Promise<User[]> {
    return this.http.getToPromise(this.restApiService.usersApi.getPath())
  }
}
