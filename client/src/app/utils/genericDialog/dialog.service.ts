import { Injectable } from '@angular/core';
import {MatDialog} from "@angular/material";
import {GenericDialogComponent} from "./generic-dialog/generic-dialog.component";

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog:MatDialog) { }

  private openDialog(_title: String, _body: String) {
    this.dialog.open(GenericDialogComponent, {
      minWidth: 100,
      maxWidth: 500,
      width: '80%',
      data: {title: _title, body: _body}
    });
  }

  openConfirmDialog(text:String, title:String = "Confirm") {
    this.openDialog(title, text)
  }

  openErrorDialog() {
    this.openDialog("Error", "Opsss this is embarassing...an error has occurred, please try again later")
  }
}
