import { HttpHeaders } from '@angular/common/http';

export interface HttpOptions {
  headers: HttpHeaders;
}

export class HttpOptionsImpl implements HttpOptions {

  headers: HttpHeaders;

  constructor(headers = { 'Content-Type': 'application/json' }) {
    this.headers = new HttpHeaders(headers);
  }

  // HttpHeaders obj is immutable!
  addHeader(name: string, value: string) {
    this.headers = this.headers.append(name, value);
  }

  addAuthToken() {
    let token = JSON.parse(localStorage.getItem("currentUser")).token;
    this.addHeader('Authorization', "Bearer " + token);
    // console.log(this.headers.get('Authorization'));
  }

}

export const h: HttpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

