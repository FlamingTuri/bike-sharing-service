import { Injectable } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PreviousRouteService {

  private previousPage: String = "";
  constructor(private router: Router) {
    this.router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe((e: NavigationEnd) => {
        this.previousPage = e.url;
      });
  }

  public getPreviousRoute() {
    return this.previousPage
  }
}
