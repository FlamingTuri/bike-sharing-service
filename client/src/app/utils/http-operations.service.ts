import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpOptionsImpl, HttpOptions } from './http-options';
import { RestApiService } from './rest-api.service';
import { HttpErrorHandlerService } from './http-error-handler.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpOperationsService {

  constructor(
    private http: HttpClient,
    private httpErrorHandlerService: HttpErrorHandlerService,
    public restApiService: RestApiService
  ) { }

  createHttpOptions(addAuth): HttpOptions {
    let httpOptions = new HttpOptionsImpl();
    if (addAuth) {
      httpOptions.addAuthToken();
    }
    return httpOptions;
  }

  /**
   * Makes a GET http request
   *
   * @param url
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  get<T>(url: string, addAuth: boolean = true): Observable<T> {
    return this.http.get<T>(url, this.createHttpOptions(addAuth));
  }

  /**
   * Makes a GET http request returning the result as a promise
   *
   * @param url
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  getToPromise<T>(url: string, addAuth: boolean = true): Promise<T> {
    return this.get<T>(url, addAuth).toPromise();
  }

  /**
   * Makes a POST http request
   *
   * @param url request's url
   * @param body request's body
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  post<T>(url: string, body, addAuth: boolean = true): Observable<T> {
    return this.http.post<T>(url, body, this.createHttpOptions(addAuth));
  }

  /**
   * Makes a POST http request returning the result as a promise
   *
   * @param url request's url
   * @param body request's body
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  postToPromise<T>(url: string, body, addAuth: boolean = true): Promise<T> {
    return this.post<T>(url, body, addAuth).toPromise();
  }

  /**
   * Makes a PUT http request
   *
   * @param url request's url
   * @param body request's body
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  put<T>(url: string, body, addAuth: boolean = true): Observable<T> {
    return this.http.put<T>(url, body, this.createHttpOptions(addAuth));
  }

  /**
   * Makes a PUT http request returning the result as a promise
   *
   * @param url request's url
   * @param body request's body
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  putToPromise<T>(url: string, body, addAuth: boolean = true): Promise<T> {
    return this.put<T>(url, body, addAuth).toPromise();
  }

  /**
   * Makes a DELETE http request
   *
   * @param url request's url
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  delete<T>(url: string, addAuth: boolean = true): Observable<T> {
    return this.http.delete<T>(url, this.createHttpOptions(addAuth));
  }

  /**
   * Makes a DELETE http request returning the result as a promise
   *
   * @param url request's url
   * @param addAuth specifies if authentication token needs to be inserted in the request's header. Default true
   */
  deleteToPromise<T>(url: string, addAuth: boolean = true): Promise<T> {
    return this.delete<T>(url, addAuth).toPromise();
  }
}
