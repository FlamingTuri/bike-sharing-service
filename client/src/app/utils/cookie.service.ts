import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CookieService {

  defaultName = 'currentUser';

  /**
   * Sets a new cookie
   * @param cname name of the new cookie
   * @param cvalue value of the new cookie
   * @param expirationDays number of days before it expires
   */
  setCookie(cname: string, cvalue, expirationDays) {
    let date = new Date();
    date.setTime(date.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + date.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  /**
   * Gets a cookie
   * @param cname the name of the cookie to get
   */
  getCookie(cname: string) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  /**
   * Checks for a cookie existance
   * @param cname the name of the cookie to check
   */
  checkCookie(cname: string) {
    let user = this.getCookie(cname);
    if (user == "") {
      this.setCookie(cname, user, 1);
    }
  }

  /**
   * Deletes a cookie setting the expire date to a passed one
   * @param cname the name of the cookie to delete
   */
  deleteCookie(cname: string) {
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  }

}
