import { Injectable, OnDestroy } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Subscription } from 'rxjs';
import { ResizeService } from './resize.service';

@Injectable({
  providedIn: 'root'
})
export class CustomSnackBarService implements OnDestroy {

  isTablet: boolean;
  subscriber: Subscription;

  constructor(
    private resizeService: ResizeService,
    public snackBar: MatSnackBar
  ) {
    this.isTablet = this.resizeService.isTablet;
    this.subscriber = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
    });
  }

  openSnackBar(message: string, duration: number = 1500, action: string = "ok") {
    let snackBarConfig: MatSnackBarConfig = {
      duration: duration,
      // must be defined globally in style.css
      panelClass: ['custom-mat-snackbar'],
      verticalPosition: this.isTablet ? 'bottom' : 'top'
    }
    this.snackBar.open(message, action, snackBarConfig);
  }

  ngOnDestroy(): void {
    this.subscriber.unsubscribe();
  }
}
