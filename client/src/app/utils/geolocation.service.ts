import { Injectable, NgZone } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';

declare var google: any;

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {

  geocoder: any;
  geoOptions: PositionOptions = {
    enableHighAccuracy: true,
    maximumAge: 5000, // 5 seconds
    timeout: 4000 // 4 seconds
  };
  positionId; // used to store the watch position id for the unsubscribe operation

  constructor(
    public mapsApiLoader: MapsAPILoader,
    private zone: NgZone,
    private wrapper: GoogleMapsAPIWrapper) {
    this.mapsApiLoader = mapsApiLoader;
    this.zone = zone;
    this.wrapper = wrapper;
    this.mapsApiLoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
    });
  }

  isGeolocationSupported(): boolean {
    return navigator.geolocation != null;
  }

  getGeolocation(): Geolocation {
    return navigator.geolocation;
  }

  getCurrentPosition(): Promise<any> {
    let self = this;
    return new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(resolve, reject, self.geoOptions);
    });
  }

  watchPosition(): Promise<any> {
    let self = this;
    return new Promise(function (resolve, reject) {
      self.positionId = navigator.geolocation.watchPosition(resolve, reject, self.geoOptions);
    });
  }

  clearWatch() {
    if (this.positionId != null) {
      navigator.geolocation.clearWatch(this.positionId);
    }
  }

  showError(error) {
    // TODO: handle errors
    switch (error.code) {
      case error.PERMISSION_DENIED:
        console.log("User denied the request for Geolocation.");
        break;
      case error.POSITION_UNAVAILABLE:
        console.log("Location information is unavailable.");
        break;
      case error.TIMEOUT:
        console.log("The request to get user location timed out.");
        break;
      default:
        console.log("An unknown error occurred.");
        break;
    }
  }
}
