import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';
import {RestApiService} from "./rest-api.service";

@Injectable({
  providedIn: 'root'
})
export class WsService {

  constructor() { }

  private subject: Rx.Subject<MessageEvent>;
  private url: String = "wss://" + window.location.hostname + ":8443/api/subscribe";

  public connect(): Rx.Subject<MessageEvent> {
    if (!this.subject) {
      this.subject = this.create(this.url);
      console.log("Successfully connected: " + this.url);
    }
    return this.subject;
  }

  private create(url): Rx.Subject<MessageEvent> {
    let ws = new WebSocket(url);

    let observable = Rx.Observable.create(
      (obs: Rx.Observer<MessageEvent>) => {
        ws.onmessage = obs.next.bind(obs);
        ws.onerror = obs.error.bind(obs);
        ws.onclose = obs.complete.bind(obs);
        return ws.close.bind(ws);
      });
    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };
    return Rx.Subject.create(observer, observable);
  }
}
