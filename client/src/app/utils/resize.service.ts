import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResizeService {

  private resizeSubject: Subject<Window>;

  constructor(private eventManager: EventManager) {
    this.resizeSubject = new Subject();
    this.eventManager.addGlobalEventListener('window', 'resize', this.onResize.bind(this));
  }

  private onResize(event: UIEvent) {
    this.resizeSubject.next(<Window>event.target);
  }

  get currentWindowsWidth(): number {
    return window.innerWidth;
  }

  get isMobile(): boolean {
    return window.innerWidth <= 600;
  }

  get isTablet(): boolean {
    return window.innerWidth <= 768;
  }

  get onResize$(): Observable<Window> {
    return this.resizeSubject.asObservable();
  }

  /*@HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(event.target.innerWidth);
  }*/

}
