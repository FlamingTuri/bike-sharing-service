import { Injectable } from '@angular/core';
import { Uri, UriImpl } from '../model/uri';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  // when debugging from computer must be localhost
  // to run from mobile replace this with your local ip 192.168.X.X
  private ip = window.location.hostname;
  private port = '8443';
  private host: Uri = new UriImpl('https://' + this.ip + ':' + this.port);
  private api: Uri = this.host.addElementToUri('api');

  loginApi: Uri = this.api.addElementToUri('login');
  registrationApi: Uri = this.api.addElementToUri('register');
  usersApi: Uri = this.api.addElementToUri('users');
  stationsApi: Uri = this.api.addElementToUri('stations');
  towersApi: Uri = this.api.addElementToUri('towers');
  bikesApi: Uri = this.api.addElementToUri('bikes');
  statsApi: Uri = this.api.addElementToUri('stats');
  bikeTypesApi: Uri = this.bikesApi.addElementToUri('types');
}
