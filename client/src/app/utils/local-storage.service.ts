import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  /* saving sensitive data in localStorage is highly insecure:
  any website can read localStorage since it is shared between pages */

  defaultKey = 'currentUser';
  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  setItem(key: string = this.defaultKey, value: string | any) {
    if (typeof value !== "string") {
      value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
  }

  getItem(key: string = this.defaultKey): string {
    return localStorage.getItem(key);
  }

  getParsedItem(key: string = this.defaultKey): any {
    return JSON.parse(this.getItem(key));
  }

  get getAuthToken(): string {
    return this.getParsedItem().token;
  }

  get isAdmin(): boolean {
    return this.getParsedItem().user.admin === 'true';
  }

  get getUserId(): string {
    return this.getParsedItem().user.id;
  }
}
