import { TestBed, inject } from '@angular/core/testing';

import { CustomSnackBarService } from './custom-snack-bar.service';

describe('CustomSnackBarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomSnackBarService]
    });
  });

  it('should be created', inject([CustomSnackBarService], (service: CustomSnackBarService) => {
    expect(service).toBeTruthy();
  }));
});
