import { Component, OnInit } from '@angular/core';
import {Marker, MarkerImpl} from "../model/marker";
import {BikeService} from "../bikes/bike.service";
import {StationService} from "../manage/stations/station.service";
import {WsService} from "../utils/ws.service";
import {Bikes} from "../bikes/bikes";
import {Station} from "../manage/stations/station";

@Component({
  selector: 'app-manager-map',
  templateUrl: './manager-map.component.html',
  styleUrls: ['./manager-map.component.css']
})
export class ManagerMapComponent implements OnInit {

  ALL: String = "a";
  markerTypeSelected: String = this.ALL;
  typeSelected: String = this.ALL;
  markers: Marker[] = [];
  bikes: Bikes[] = [];
  stations: Station[] = [];

  constructor(private dataB: BikeService,
              private dataS: StationService,
              private ws: WsService) {
    dataS.getStations().then(res => {
      this.stations = res;
      this.updateMarker();
    }).catch(err => console.log("GET stations: " + err));

    dataB.getBikes().then(res => {
      this.bikes = res;
      this.updateMarker();
    }).catch(err => console.log("GET bikes: " + err));

    //ws update
    ws.connect().subscribe(m => {
      let msg = JSON.parse(m.data);
      if(msg.type == 1) {
        let s = this.markers.find(s => s.type == 'station' && s.data.id == msg.data.id);
        if(s != undefined) {
          s.data.availableBikes = msg.data.availableBikes;
        }
      } else {
        let bike = this.markers.find(b => b.type == 'bike' && b.data.id == msg.data.id);
        if (bike != undefined) {
          bike.lat = msg.data.location.lat;
          bike.lng = msg.data.location.long;
        }
      }
    })
  }

  ngOnInit() {
  }

  updateMarker() {
    this.markers = [];
    if (this.markerTypeSelected == this.ALL || this.markerTypeSelected == 's') {
      this.stations.filter(s => this.typeSelected == this.ALL || this.typeSelected == s.type.name).forEach(element => {
        let location = element.location;
        this.markers.push(new MarkerImpl(location.lat.valueOf(), location.long.valueOf(), element, true, "station"));
      });
    }

    if (this.markerTypeSelected == this.ALL || this.markerTypeSelected == 'b') {
      this.bikes.filter(b => this.typeSelected == this.ALL || this.typeSelected == b.type.name).forEach(element => {
        let location = element.currLocation;
        this.markers.push(new MarkerImpl(location.lat.valueOf(), location.long.valueOf(), element, true, "bike"));
      });
    }
  }

}
