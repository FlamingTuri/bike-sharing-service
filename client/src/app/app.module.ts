import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { NavigationComponent } from './navigation/navigation.component';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { MapComponent } from './map/map.component';
import { AgmDirectionModule } from 'agm-direction';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BikesComponent } from './bikes/bikes.component';
import { AppRoutingModule } from './/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './custom-modules/custom-material.module'
import { LoginComponent } from './login-registration/login/login.component'
import { RegistrationComponent } from './login-registration/registration/registration.component'
import { BikeRowComponent } from './bikes/bike-row/bike-row.component';
import { BikeDetailsComponent } from './bike-details/bike-details.component';
import { ManageComponent } from './manage/manage.component';
import { HomeComponent } from './user/home/home.component';
import { ProfileComponent } from './user/profile/profile.component';
import { UserComponent } from './user/user.component';
import { DialogRepairComponent } from "./bike-details/dialog-repair/dialog-repair.component";
import { DialogAvailableComponent } from './bike-details/dialog-available/dialog-available.component';
import { BadgesComponent } from './user/badges/badges.component';
import { ServiceInfoComponent } from './user/service-info/service-info.component';
import { FloatingButtonComponent } from './floating-button/floating-button.component';
import { DialogAddBikeComponent } from './bikes/dialog-add-bike/dialog-add-bike.component';
import { StationsComponent } from './manage/stations/stations.component';
import { StationRowComponent } from './manage/stations/station-row/station-row.component';
import { StationDetailsComponent } from './manage/stations/station-details/station-details.component';
import { BadgeInfoDialogComponent } from './user/badges/badge-info-dialog/badge-info-dialog.component';
import { ContainerComponent } from './user/badges/container/container.component';
import { DialogAddStationComponent } from './manage/stations/dialog-add-station/dialog-add-station.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ListUsersComponent } from './list-users/list-users.component';
import { UserRowComponent } from './list-users/user-row/user-row.component';
import { UserDetailsComponent } from './list-users/user-details/user-details.component';
import { ScannerComponent } from './user/home/scanner/scanner.component';
import { RentReturnBikeComponent } from './user/home/rent-return-bike/rent-return-bike.component';
import { DialogLogComponent } from './bike-details/dialog-log/dialog-log.component';
import { BottomSheetComponent } from './show-map-details/bottom-sheet/bottom-sheet.component';
import { ShowMapDetailsComponent } from './show-map-details/show-map-details.component';
import { ManagerMapComponent } from './manager-map/manager-map.component';
import { StatisticComponent } from './statistic/statistic.component';
import { GenericDialogComponent } from './utils/genericDialog/generic-dialog/generic-dialog.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginRegistrationComponent,
    LoginComponent,
    RegistrationComponent,
    NavigationComponent,
    MapComponent,
    BikesComponent,
    PageNotFoundComponent,
    BikeRowComponent,
    BikeDetailsComponent,
    ManageComponent,
    HomeComponent,
    ProfileComponent,
    UserComponent,
    DialogRepairComponent,
    DialogAvailableComponent,
    BadgesComponent,
    ServiceInfoComponent,
    FloatingButtonComponent,
    DialogAddBikeComponent,
    StationsComponent,
    StationRowComponent,
    StationDetailsComponent,
    StationRowComponent,
    BadgeInfoDialogComponent,
    ContainerComponent,
    DialogAddStationComponent,
    ListUsersComponent,
    UserRowComponent,
    UserDetailsComponent,
    DialogAddStationComponent,
    ScannerComponent,
    RentReturnBikeComponent,
    DialogLogComponent,
    BottomSheetComponent,
    ShowMapDetailsComponent,
    ManagerMapComponent,
    StatisticComponent,
    GenericDialogComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBRZ-T0jHDDDXMdrFJD9yaguASXk1Ugoo4',
      libraries: ['geometry']
    }),
    AgmDirectionModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    CustomMaterialModule,
    ZXingScannerModule.forRoot()
  ],
  entryComponents: [
    DialogRepairComponent,
    DialogAvailableComponent,
    DialogAddBikeComponent,
    BadgeInfoDialogComponent,
    DialogLogComponent,
    GenericDialogComponent,
    BottomSheetComponent
  ],
  providers: [GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
