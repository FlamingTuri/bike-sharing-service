import { Component, OnInit, OnDestroy } from '@angular/core';
import { RentReturnBikeService } from './rent-return-bike.service';
import { CustomSnackBarService } from 'src/app/utils/custom-snack-bar.service';
//import { Subscription } from 'rxjs';
//import { ScannerDataNotifierService } from '../scanner-data-notifier.service';

@Component({
  selector: 'app-rent-return-bike',
  templateUrl: './rent-return-bike.component.html',
  styleUrls: ['./rent-return-bike.component.css']
})
export class RentReturnBikeComponent implements OnInit, OnDestroy {

  //subscription: Subscription;
  bikeId: string;
  bikeRented: boolean = false;
  code: string = '';
  errorMsg: string;
  isCreditCardSet: boolean = false;

  constructor(
    private rentReturnBikeService: RentReturnBikeService,
    //private scannerDataNotifierService: ScannerDataNotifierService,
    private customSnackBarService: CustomSnackBarService
  ) {
    /*this.subscription = scannerDataNotifierService.scannerResult$.subscribe(code => {
      this.code = code;
    });*/
  }

  ngOnInit() {
    this.rentReturnBikeService.getUserBike().then(result => {
      if (JSON.stringify(result) != "{}") {
        console.log(result);
        this.bikeRented = true;
        this.bikeId = result.bikeId;
      }
    }).catch(response => {
      console.log(response);
      this.errorMsg = response.error;
    });

    this.rentReturnBikeService.getUserInfo().then(result => {
      //console.log(result);
      let creditCardInfo = result.creditCardInfo;
      if (creditCardInfo) {
        if (!(this.notSet(creditCardInfo.name) ||
          this.notSet(creditCardInfo.cardNumber) ||
          this.notSet(creditCardInfo.cvCode) ||
          this.notSet(creditCardInfo.expirationDate))) {
          this.isCreditCardSet = true;
        }
      }
    }).catch(response => {
      console.log(response);
      this.errorMsg = response.error;
    });
  }

  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

  notSet(param) {
    return param == '' || param == null;
  }

  resetUI() {
    this.code = '';
    this.errorMsg = '';
  }

  areBikeOperationsEnabled(): boolean {
    return this.code != '' && this.code != null && this.code != undefined;
  }

  rentBike() {
    if (this.isCreditCardSet) {
      this.rentReturnBikeService.rentBike(this.code).then(result => {
        // console.log(result);
        this.bikeId = result.id;
        this.bikeRented = true;
        this.resetUI();
        this.customSnackBarService.openSnackBar('bike rented successfully');
      }).catch(response => {
        console.log(response);
        this.errorMsg = response.error;
      });
    } else {
      this.errorMsg = 'you must set credit card to be able to rent a bike';
    }
  }

  returnBike() {
    this.rentReturnBikeService.returnBike(this.code, this.bikeId).then(result => {
      console.log(result);
      this.bikeRented = false;
      this.resetUI();
      let message = 'bike returned successfully, you will be charged of ' + result.totalCost.toFixed(2) + "€";
      this.customSnackBarService.openSnackBar(message, 3000);
    }).catch(response => {
      console.log(response);
      this.errorMsg = response.error;
    });
  }

  notifyBreakdown() {
    this.rentReturnBikeService.notifyBreakDown(this.code).then(result => {
      this.resetUI();
    }).catch(response => {
      console.log(response);
      this.errorMsg = response.error.message;
    });
  }
}
