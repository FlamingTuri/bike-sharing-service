import { TestBed, inject } from '@angular/core/testing';

import { RentReturnBikeService } from './rent-return-bike.service';

describe('RentReturnBikeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RentReturnBikeService]
    });
  });

  it('should be created', inject([RentReturnBikeService], (service: RentReturnBikeService) => {
    expect(service).toBeTruthy();
  }));
});
