import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentReturnBikeComponent } from './rent-return-bike.component';

describe('RentReturnBikeComponent', () => {
  let component: RentReturnBikeComponent;
  let fixture: ComponentFixture<RentReturnBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentReturnBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentReturnBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
