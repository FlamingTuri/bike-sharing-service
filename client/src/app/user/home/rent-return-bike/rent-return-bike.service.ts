import { Injectable } from '@angular/core';
import { RestApiService } from '../../../utils/rest-api.service';
import { HttpOperationsService } from '../../../utils/http-operations.service';
import { Uri } from 'src/app/model/uri';
import { LocalStorageService } from 'src/app/utils/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class RentReturnBikeService {

  userId: string = this.localStorageService.getUserId;
  usersUrl: Uri = this.restApiService.usersApi;
  towersUrl: Uri = this.restApiService.towersApi;

  constructor(
    private httpOperationsService: HttpOperationsService,
    private restApiService: RestApiService,
    private localStorageService: LocalStorageService
  ) { }

  getUserBike(): Promise<any> {
    let url: string = this.usersUrl.addElementToUri(this.userId).addElementToUri('bike').getPath();
    return this.httpOperationsService.getToPromise(url);
  }

  rentBike(towerCode: string): Promise<any> {
    let body = {
      userId: this.userId
    };
    let url: string = this.towersUrl.addElementToUri(towerCode).addElementToUri('rent-bike').getPath();
    return this.httpOperationsService.postToPromise(url, body);
  }

  returnBike(towerCode: string, bikeId: string): Promise<any> {
    let body = {
      userId: this.userId,
      bikeId: bikeId
    };
    let url: string = this.towersUrl.addElementToUri(towerCode).addElementToUri('return-bike').getPath();
    return this.httpOperationsService.postToPromise(url, body);
  }

  notifyBreakDown(bikeId: string): Promise<any> {
    let body = {
      userId: this.userId,
      bike: bikeId
    };
    throw new Error("unimplemented");
  }

  getUserInfo(): Promise<any> {
    let url: string =  this.restApiService.usersApi.addElementToUri(this.userId).getPath();
    return this.httpOperationsService.getToPromise(url);
  }
}
