import { TestBed, inject } from '@angular/core/testing';

import { ScannerDataNotifierService } from './scanner-data-notifier.service';

describe('ScannerDataNotifierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScannerDataNotifierService]
    });
  });

  it('should be created', inject([ScannerDataNotifierService], (service: ScannerDataNotifierService) => {
    expect(service).toBeTruthy();
  }));
});
