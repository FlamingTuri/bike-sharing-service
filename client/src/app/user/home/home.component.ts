import { Component, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { Marker, MarkerImpl } from '../../model/marker';
import { GeolocationService } from '../../utils/geolocation.service'
import { ResizeService } from 'src/app/utils/resize.service';
import { MapMarkerService } from '../../map/map-marker.service';
import { Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material';
import { ScannerComponent } from './scanner/scanner.component';
import { RentReturnBikeComponent } from './rent-return-bike/rent-return-bike.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  markers: Marker[] = [];
  userLocation: Marker;
  isTabletSubscriber: Subscription;
  isTablet: boolean;
  mapHeight = 300;

  @ViewChild(RentReturnBikeComponent) private scannerComponent: RentReturnBikeComponent;
  qrActive: boolean = false;
  toScanModeText = "switch to scan mode";
  toManualModeText = "switch to manual insert mode";
  switchViewButtonText = this.toManualModeText;

  constructor(
    public geolocationService: GeolocationService,
    public resizeService: ResizeService,
    private mapMarkerService: MapMarkerService,
  ) {
    this.isTabletSubscriber = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
    });
  }

  ngOnInit() {
    this.isTablet = this.resizeService.isTablet;

    this.mapMarkerService.getStations().then(result => {
      //console.log(result);

      result.forEach(element => {
        let location = element.location;
        this.markers.push(new MarkerImpl(location.lat, location.long, element, false, "station"));
      });

    }).then(() => {
      this.userLocation = new MarkerImpl(44.1391, 12.24315);
      this.userLocation.description = "your position";
      this.markers.push(this.userLocation);
      // retrieving user current location
      this.geolocationService.getCurrentPosition().then((position) => {
        this.userLocation.lat = position.coords.latitude;
        this.userLocation.lat = position.coords.longitude;
        // console.log(this.userLocation);
      }).then(() => {
        // keeping track of user location
        this.geolocationService.watchPosition().then((position) => {
          this.userLocation.lat = position.coords.latitude;
          this.userLocation.lng = position.coords.longitude;
          // console.log(this.userLocation);
        });
      }).catch(error => {
        console.log(error);
      });
    }).catch(error => {
      console.log(error);
    });

  }

  getHtmlSelectorHeight(selector: string): number {
    let element = document.querySelector(selector);
    return parseInt(getComputedStyle(element).height, 10);
  }

  ngOnDestroy() {
    this.geolocationService.clearWatch();
    this.isTabletSubscriber.unsubscribe();
  }

  onCodeRetrieval(code: string) {
    //console.log(code);
    this.scannerComponent.code = code;
    this.switchView();
  }

  switchView() {
    this.qrActive = !this.qrActive;
    this.switchViewButtonText = this.qrActive ? this.toManualModeText : this.toScanModeText;
  }

  onTabChange(event: MatTabChangeEvent) {
    switch (event.tab.textLabel) {
      case "Map":
        this.qrActive = false;
        break;
      case "Rent Bike":
        this.qrActive = true;
        break;
    }
  }

}
