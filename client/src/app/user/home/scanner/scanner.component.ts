import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';
import { CustomSnackBarService } from 'src/app/utils/custom-snack-bar.service';
//import { ScannerDataNotifierService } from '../scanner-data-notifier.service';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.css']
})
export class ScannerComponent implements OnInit {

  @ViewChild('scanner')
  scanner: ZXingScannerComponent;

  @Input() qrActive: boolean;
  @Output() codeRetrieved = new EventEmitter<string>();

  hasCameras = false;
  hasPermission: boolean;
  qrResultString: string;

  availableDevices: MediaDeviceInfo[];
  selectedDevice: MediaDeviceInfo;
  selected = "No Device";

  constructor(
    //private scannerDataNotifierService: ScannerDataNotifierService,
    private customSnackBarService: CustomSnackBarService
  ) { }

  ngOnInit() {
    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.hasCameras = true;

      //console.log('Devices: ', devices);
      this.availableDevices = devices;

      // selects the devices's back camera by default
      let device = devices.find(device => /back|rear|environment/gi.test(device.label));
      if (device) {
        console.log("ok");
        this.scanner.changeDevice(device);
        this.selected = device.deviceId;
        this.selectedDevice = device;
      }

      /*for (const device of devices) {
        if (/back|rear|environment/gi.test(device.label)) {
          alert("ok");
          this.scanner.changeDevice(device);
          this.selected = device.deviceId;
          this.selectedDevice = device;
          break;
        }
      }*/
    });

    this.scanner.camerasNotFound.subscribe((devices: MediaDeviceInfo[]) => {
      console.error('An error has occurred when trying to enumerate your video-stream-enabled devices.');
    });

    this.scanner.permissionResponse.subscribe((answer: boolean) => {
      this.hasPermission = answer;
    });
  }

  handleQrCodeResult(resultString: string) {
    //console.log('Result: ', resultString);
    console.log(this.qrActive);
    if (this.qrActive) {
      this.qrResultString = resultString;
      // updates the code field of rent-return-bike.component
      this.codeRetrieved.emit(resultString);
      //this.scannerDataNotifierService.notifyScannerResult(resultString);
      let message = 'code identified';
      this.customSnackBarService.openSnackBar(message, 3000);
    }
  }

  onDeviceSelectChange(selectedValue) {
    //console.log('Selection changed: ', selectedValue);
    this.selectedDevice = this.scanner.getDeviceById(selectedValue.value);
  }

}
