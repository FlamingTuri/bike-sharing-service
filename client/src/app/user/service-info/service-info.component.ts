import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/utils/rest-api.service';
import { HttpOperationsService } from 'src/app/utils/http-operations.service';

@Component({
  selector: 'app-service-info',
  templateUrl: './service-info.component.html',
  styleUrls: ['./service-info.component.css']
})
export class ServiceInfoComponent implements OnInit {

  bikeTypes;

  constructor(
    private restApiService: RestApiService,
    private httpOperations: HttpOperationsService
  ) { }

  ngOnInit() {
    let url = this.restApiService.bikeTypesApi.getPath()
    this.httpOperations.getToPromise(url).then(result => {
      console.log(result);
      this.bikeTypes = result;
    }).catch(response => console.log(response));
  }

}
