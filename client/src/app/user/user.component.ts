import { Component, OnInit } from '@angular/core';
import { NavItem } from '../model/nav-item';
import { LocalStorageService } from '../utils/local-storage.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  sideNavContent: NavItem[] = [];

  userItems: NavItem[] = [
    {
      nameDisplayed: "Home",
      routerLink: "/user/home",
      icon: "home"
    },
    {
      nameDisplayed: "Profile",
      routerLink: "/user/profile",
      icon: "account_circle"
    },
    {
      nameDisplayed: "Badges",
      routerLink: "/user/badges",
      icon: "stars"
    },
    {
      nameDisplayed: "Info",
      routerLink: "/user/serviceinfo",
      icon: "info"
    }
  ];

  adminItems: NavItem[] = [
    {
      nameDisplayed: "Map",
      routerLink: "/user/map",
      icon: "explore"
    },{
      nameDisplayed: "Manage",
      routerLink: "/user/manage",
      icon: "build"
    },
    {
      nameDisplayed: "Users",
      routerLink: "/user/users",
      icon: "supervised_user_circle"
    },
    {
      nameDisplayed: "Statistic",
      routerLink: "/user/statistic",
      icon: "bar_chart"
    }
  ];

  logoutItem: NavItem = {
    nameDisplayed: "Logout",
    routerLink: "/authentication/login",
    icon: "exit_to_app"
  };

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.sideNavContent = this.userItems;
    if (this.localStorageService.isAdmin) {
      this.sideNavContent = this.sideNavContent.concat(this.adminItems);
    }
    this.sideNavContent.push(this.logoutItem);
  }

}
