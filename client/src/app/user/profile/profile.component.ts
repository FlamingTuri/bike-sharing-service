import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UpdateProfileService } from './update-profile.service';
import { PersonalInfo } from '../../model/personal-info';
import { CreditCard } from '../../model/credit-card';
import { CustomSnackBarService } from '../../utils/custom-snack-bar.service';
import { ResizeService } from 'src/app/utils/resize.service';
import { Subscription } from 'rxjs';
import { LocalStorageService } from 'src/app/utils/local-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  @Input() userId: string = this.localStorageService.getUserId;
  @Input() isReadonly: boolean = false;

  subscriber: Subscription;
  isTablet: boolean;

  username: string;

  private serverPersonalInfo: PersonalInfo = {};
  profileErrorMsg: string;
  personalInfoFormGroup: FormGroup;

  private hide = true;
  passwordErrorMsg: string;
  passwordChangeFormGroup: FormGroup;

  private serverEmail: string;
  emailErrorMsg: string;
  emailChangeFormGroup: FormGroup;

  private serverCreditCard: CreditCard;
  creditCardErrorMsg: string;
  creditCardChangeFormGroup: FormGroup;

  constructor(
    private resizeService: ResizeService,
    private matIconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private updateProfileService: UpdateProfileService,
    private fb: FormBuilder,
    private customSnackBarService: CustomSnackBarService,
    private localStorageService: LocalStorageService
  ) {
    this.matIconRegistry.addSvgIcon(
      'create',
      sanitizer.bypassSecurityTrustResourceUrl('../assets/images/baseline-create-24px.svg')
    );
  }

  getDefaultRequiredControl(formState = '', validators = [Validators.required]): FormControl {
    return new FormControl(formState, validators)
  }

  ngOnInit() {
    this.isTablet = this.resizeService.isTablet;

    this.subscriber = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
    });

    this.personalInfoFormGroup = this.fb.group({
      firstname: this.getDefaultRequiredControl(),
      lastname: this.getDefaultRequiredControl(),
      birthday: '',
      country: '',
      city: '',
    });
    this.passwordChangeFormGroup = this.fb.group({
      newPassword: this.getDefaultRequiredControl(),
      newPasswordConfirmation: this.getDefaultRequiredControl(),
    });
    this.emailChangeFormGroup = this.fb.group({
      newEmail: this.getDefaultRequiredControl(),
    });
    this.creditCardChangeFormGroup = this.fb.group({
      name: this.getDefaultRequiredControl(),
      cardNumber: this.getDefaultRequiredControl(),
      expirationDate: this.getDefaultRequiredControl(),
      cvCode: this.getDefaultRequiredControl(),
    });

    /* gets user data stored in the server */
    this.updateProfileService.setUser(this.userId);
    this.updateProfileService.getUserInfo().then(result => {
      console.log(result);

      this.username = result.username;

      this.serverPersonalInfo.firstname = result.name;
      this.serverPersonalInfo.lastname = result.surname;
      /*this.serverPersonalInfo.birthday = result.birthday;*/
      this.serverPersonalInfo.country = result.country;
      this.serverPersonalInfo.city = result.city;

      this.personalInfoFormGroup = this.fb.group({
        firstname: [result.name, Validators.required],
        lastname: [result.surname, Validators.required],
        /*birthday: '',*/
        country: result.country,
        city: result.city,
      });

      this.serverEmail = result.email;
      this.emailChangeFormGroup = this.fb.group({
        newEmail: this.getDefaultRequiredControl(result.email, [Validators.required, Validators.email]),
      });

      if (result.creditCardInfo) {
        this.serverCreditCard = result.creditCardInfo;
        this.creditCardChangeFormGroup = this.fb.group({
          name: this.getDefaultRequiredControl(result.creditCardInfo.name),
          cardNumber: this.getDefaultRequiredControl(result.creditCardInfo.cardNumber),
          expirationDate: this.getDefaultRequiredControl(result.creditCardInfo.expirationDate),
          cvCode: this.getDefaultRequiredControl(result.creditCardInfo.cvCode),
        });
      }
    }).catch(error => {
      console.log(error);
      this.customSnackBarService.openSnackBar(error.message);
    });
  }

  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }

  personalInfoAreUnchanged(): boolean {
    return this.jsonIsEqual(this.serverPersonalInfo, this.personalInfoFormGroup.value);
  }

  savePersonalInfoChanges() {
    let personalInfoValue = this.personalInfoFormGroup.value;
    this.updateProfileService.updateUserInfo(personalInfoValue).then(() => {
      this.serverPersonalInfo = personalInfoValue;
      this.customSnackBarService.openSnackBar("info updated successfully");
    }).catch(error => {
      this.customSnackBarService.openSnackBar(error.message);
    });
  }

  checkPasswordConfirm(): boolean {
    let formValue = this.passwordChangeFormGroup.value;
    return formValue.newPassword == formValue.newPasswordConfirmation;
  }

  savePasswordChange() {
    let newPassword: string = this.passwordChangeFormGroup.value.newPassword;
    if (newPassword.length < 8) {
      this.passwordErrorMsg = 'Passwords should be at least 8 characters long';
    } else if (this.checkPasswordConfirm()) {
      this.updateProfileService.updateUserPassword(newPassword).then(() => {
        this.customSnackBarService.openSnackBar("password updated successfully");
      }).catch(error => {
        this.customSnackBarService.openSnackBar(error.message);
      });
    } else {
      this.passwordErrorMsg = 'Passwords are not the same';
    }
  }

  emailIsUnchanged(): boolean {
    return this.serverEmail == this.emailChangeFormGroup.value.newEmail;
  }

  saveEmailChange() {
    let newEmail = this.emailChangeFormGroup.value.newEmail;
    this.updateProfileService.updateUserEmail(newEmail).then(result => {
      this.serverEmail = newEmail;
      this.customSnackBarService.openSnackBar("email updated successfully");
    }).catch(error => {
      console.error(error);
      this.customSnackBarService.openSnackBar("an error has occurred");
    });
  }

  creditCardIsUnchanged(): boolean {
    return this.jsonIsEqual(this.serverCreditCard, this.creditCardChangeFormGroup.value);
  }

  saveCreditCardChange() {
    let creditCardInfo = this.creditCardChangeFormGroup.value;
    this.updateProfileService.updateUserCreditCard(creditCardInfo).then(() => {
      this.serverCreditCard = creditCardInfo;
      this.customSnackBarService.openSnackBar("credit card info updated successfully");
    }).catch(error => {
      console.error(error);
      this.customSnackBarService.openSnackBar(error.message);
    });
  }

  cloneJsonObject(objectToClone): any {
    return JSON.parse(JSON.stringify(objectToClone));
  }

  jsonIsEqual(object1, object2): boolean {
    return JSON.stringify(object1) == JSON.stringify(object2);
  }
}
