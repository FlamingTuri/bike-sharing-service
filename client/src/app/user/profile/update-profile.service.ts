import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestApiService } from '../../utils/rest-api.service';
import { HttpOperationsService } from 'src/app/utils/http-operations.service';

@Injectable({
  providedIn: 'root'
})
export class UpdateProfileService {

  userId: string;// = JSON.parse(localStorage.getItem("currentUser")).user.id;
  url: string;// = this.restApiService.usersApi.addElementToUri(this.userId).getPath();

  constructor(
    private httpOperationsService: HttpOperationsService,
    private restApiService: RestApiService
  ) {
  }

  setUser(userId){
    this.url = this.restApiService.usersApi.addElementToUri(userId).getPath();
  }

  getUserInfo(): Promise<any> {
    return this.httpOperationsService.getToPromise(this.url);
  }

  updateUserInfo(personalInfo): Promise<any> {
    personalInfo.name = personalInfo.firstname;
    personalInfo.surname = personalInfo.lastname;
    return this.httpOperationsService.putToPromise(this.url, personalInfo);
  }

  updateUserPassword(newPassword): Promise<any> {
    let body = {
      password: newPassword
    };
    return this.httpOperationsService.putToPromise(this.url, body);
  }

  updateUserEmail(newEmail): Promise<any> {
    let body = {
      email: newEmail
    };
    return this.httpOperationsService.putToPromise(this.url, body);
  }

  updateUserCreditCard(creditCardInfo): Promise<any> {
    let body = {
      creditCardInfo: creditCardInfo
    };
    return this.httpOperationsService.putToPromise(this.url, body);
  }

}
