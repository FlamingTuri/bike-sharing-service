import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { BadgesService } from '../badges.service';
import { MatDialog } from '@angular/material';
import { BadgeInfoDialogComponent } from '../badge-info-dialog/badge-info-dialog.component';
import { Badge } from '../../../model/badge';
import { ResizeService } from 'src/app/utils/resize.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() badges: Badge[];
  @Input() achieved: boolean = true;

  isTablet: Boolean;
  badgeCols = 1;
  badgeRows = 1;

  constructor(
    badgesService: BadgesService,
    private resizeService: ResizeService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.isTablet = this.resizeService.isTablet;
    this.resizeService.onResize$.subscribe(size => {
      this.isTablet = this.resizeService.isTablet;
    })
  }

  ngOnDestroy(): void {
  }

  openBadgeDetailsDialog(badge: Badge) {
    console.log(badge);
    const dialogRef = this.dialog.open(BadgeInfoDialogComponent, {
      width: '500px',
      data: badge
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      if (result) {
        // TODO: notify redeem
        console.log(result);
      }
    });
  }

}
