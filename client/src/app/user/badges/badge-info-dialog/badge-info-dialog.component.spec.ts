import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeInfoDialogComponent } from './badge-info-dialog.component';

describe('BadgeInfoDialogComponent', () => {
  let component: BadgeInfoDialogComponent;
  let fixture: ComponentFixture<BadgeInfoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgeInfoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgeInfoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
