import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Badge } from '../../../model/badge';

@Component({
  selector: 'app-badge-info-dialog',
  templateUrl: './badge-info-dialog.component.html',
  styleUrls: ['./badge-info-dialog.component.css']
})
export class BadgeInfoDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<BadgeInfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Badge
  ) { }


  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
