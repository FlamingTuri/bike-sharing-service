import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap, delay } from 'rxjs/operators';
import { HttpOptionsImpl } from '../../utils/http-options';
import { RestApiService } from '../../utils/rest-api.service';
import { HttpErrorHandlerService } from '../../utils/http-error-handler.service';
import { HttpClient } from '@angular/common/http';
import { HttpOperationsService } from 'src/app/utils/http-operations.service';

@Injectable({
  providedIn: 'root'
})
export class BadgesService {

  userId: string = JSON.parse(localStorage.getItem("currentUser")).user.id;
  url: string = this.restApiService.usersApi.addElementToUri(this.userId).addElementToUri('badges').getPath();

  constructor(
    private restApiService: RestApiService,
    private httpOperations: HttpOperationsService
  ) { }

  getUserBadges(): Promise<any> {
    return this.httpOperations.getToPromise(this.url);
  }

  /*useBadge(): Observable<boolean> {
    return this.http.put<any>(this.restApiService.loginApi.getPath(), new HttpOptionsImpl()).pipe(
      tap(result => {console.log(result)}),
      catchError(this.httpErrorHandlerService.handleError<any>())
    );
  }*/

}
