import { Component, OnInit } from '@angular/core';
import { BadgesService } from './badges.service';
import { Badge } from '../../model/badge';
import { BadgeStatus } from '../../model/badge-status';
/*
const ELEMENT_DATA: Badge[] = [
  { id: 1, icon: 1, name: 'First Login!', description: "acquired by loggin in for the first time, use this to claim a 10% discount on your next trip", status: BadgeStatus.Available },
  { id: 2, icon: 2, name: 'Invite a friend', description: "WIP", status: BadgeStatus.Available },
  { id: 3, icon: 2, name: 'Invite 5 friends', description: "WIP", status: BadgeStatus.Redeemed },
  { id: 4, icon: 2, name: 'Rent a bike for the first time', description: "WIP", status: BadgeStatus.Redeemed },
  { id: 5, icon: 2, name: 'Use the service for 1 hour', description: "WIP", status: BadgeStatus.Redeemed },
  { id: 6, icon: 3, name: 'Use the service for 5 hours', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 7, icon: 3, name: 'Use the service for 10 hours', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 8, icon: 3, name: 'Use the service for 15 hours', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 9, icon: 3, name: 'Use the service for 20 hours', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 10, icon: 3, name: 'Log in 3 days in a row', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 11, icon: 3, name: 'Log in 5 days in a row', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 12, icon: 3, name: 'Log in 7 days in a row', description: "WIP", status: BadgeStatus.Unlockable },
  { id: 13, icon: 3, name: 'Log in 14 days in a row', description: "WIP", status: BadgeStatus.Unlockable },
];*/

@Component({
  selector: 'app-badges',
  templateUrl: './badges.component.html',
  styleUrls: ['./badges.component.css']
})
export class BadgesComponent implements OnInit {

  badges;

  unlockedTitle = "Unlocked";
  lockedTitle = "Locked";

  constructor(
    private badgesService: BadgesService
  ) { }

  ngOnInit() {
    this.badgesService.getUserBadges().then(result => {
      console.log(result)
      this.badges = result;
    }).catch(error => console.log(error));
  }

}
