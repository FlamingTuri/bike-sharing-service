import { NgModule } from '@angular/core';
import { Route, Routes, RouterModule } from '@angular/router';
import { LoginRegistrationComponent } from './login-registration/login-registration.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login-registration/login/login.component'
import { RegistrationComponent } from './login-registration/registration/registration.component'
import { BikeDetailsComponent } from "./bike-details/bike-details.component";
import { ManageComponent } from "./manage/manage.component";
import { HomeComponent } from './user/home/home.component';
import { ProfileComponent } from './user/profile/profile.component';
import { UserComponent } from './user/user.component';
import { AuthGuard } from './auth/auth.guard';
import { BadgesComponent } from './user/badges/badges.component';
import { ServiceInfoComponent } from './user/service-info/service-info.component';
import { StationDetailsComponent } from "./manage/stations/station-details/station-details.component";
import { ListUsersComponent } from "./list-users/list-users.component";
import { UserDetailsComponent } from "./list-users/user-details/user-details.component";
import { DialogAddStationComponent } from "./manage/stations/dialog-add-station/dialog-add-station.component";
import { SuperUserGuard } from './auth/super-user.guard';
import {ManagerMapComponent} from "./manager-map/manager-map.component";
import {StatisticComponent} from "./statistic/statistic.component";

const defaultRoute: Route = { path: '', redirectTo: '/user/home', pathMatch: 'full' }

const authenticationRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: '', redirectTo: '/authentication/login', pathMatch: 'full' }
];

const userRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'serviceinfo', component: ServiceInfoComponent },
  { path: 'badges', component: BadgesComponent }
];

const superUserRoutes: Routes = [
  { path: 'manage', canActivate: [SuperUserGuard], component: ManageComponent },
  { path: 'addStations', canActivate: [SuperUserGuard], component: DialogAddStationComponent },
  { path: 'bikeDetails/:id', canActivate: [SuperUserGuard], component: BikeDetailsComponent },
  { path: 'stationDetails/:id', canActivate: [SuperUserGuard], component: StationDetailsComponent },
  { path: 'users', canActivate: [SuperUserGuard], component: ListUsersComponent },
  { path: 'userDetails/:id', canActivate: [SuperUserGuard], component: UserDetailsComponent },
  { path: 'map', canActivate: [SuperUserGuard], component: ManagerMapComponent },
  { path: 'statistic', canActivate: [SuperUserGuard], component: StatisticComponent }
];

const routes: Routes = [
  {
    path: 'authentication',
    component: LoginRegistrationComponent,
    children: authenticationRoutes
  },
  {
    path: 'user',
    canActivate: [AuthGuard],
    component: UserComponent,
    children: userRoutes.concat(superUserRoutes, defaultRoute)
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    component: UserComponent,
    children: userRoutes.concat(superUserRoutes, defaultRoute)
  },
  defaultRoute,
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
