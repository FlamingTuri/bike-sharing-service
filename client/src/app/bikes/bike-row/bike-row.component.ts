import {Component, Input, OnInit} from '@angular/core';
import {Bikes} from "../bikes";

@Component({
  selector: 'app-bike-row',
  templateUrl: './bike-row.component.html',
  styleUrls: ['./bike-row.component.css']
})
export class BikeRowComponent implements OnInit {

  @Input() bike: Bikes;

  constructor() { }

  ngOnInit() {
  }

}
