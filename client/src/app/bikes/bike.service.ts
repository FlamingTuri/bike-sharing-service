import { Injectable } from '@angular/core';
import {HttpOperationsService} from "../utils/http-operations.service";
import {Bikes} from "./bikes";
import {RestApiService} from "../utils/rest-api.service";
import {FreeTowers} from "../bike-details/freeTowers";

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  userId: string = JSON.parse(localStorage.getItem("currentUser")).user.id;

  constructor(private http:HttpOperationsService, private restApiService: RestApiService) { }

  getBikes(): Promise<Bikes[]>{
    return this.http.getToPromise(this.restApiService.bikesApi.getPath());
  }

  getBike(id: String): Promise<any> {
    return this.http.getToPromise(this.restApiService.bikesApi.addElementToUri(id).getPath())
  }

  startMaintenance(idBike:String, notes:String) {
    let body = {
      userId: this.userId,
      notes: notes
    };
    return this.http.postToPromise(this.restApiService.bikesApi.addElementToUri(idBike).addElementToUri("start-maintenance").getPath(),
      body);
  }

  getFreeTower(): Promise<FreeTowers[]> {
    return this.http.getToPromise(this.restApiService.stationsApi.addElementToUri("grouped").getPath())
  }

  endMaintenance(idBike:String, idTower:String) {
    let body = {
      userId: this.userId,
      towerId: idTower
    };
    return this.http.postToPromise(this.restApiService.bikesApi.addElementToUri(idBike).addElementToUri("end-maintenance").getPath(),
      body);
  }

  createBike(idType:String, idTower:String) {
    let body = {
      userId: this.userId,
      towerId: idTower,
      typeId: idType
    };
    return this.http.postToPromise(this.restApiService.bikesApi.getPath(), body);
  }
}
