import { Component, OnInit } from '@angular/core';
import { BikeService} from "./bike.service";
import {MatDialog} from "@angular/material";
import {DialogAddBikeComponent} from "./dialog-add-bike/dialog-add-bike.component";
import {ResizeService} from '../utils/resize.service';
import {Bikes} from "./bikes";
import {DialogService} from "../utils/genericDialog/dialog.service";

@Component({
  selector: 'app-bikes',
  templateUrl: './bikes.component.html',
  styleUrls: ['./bikes.component.css']
})
export class BikesComponent implements OnInit {

  bikes$: Bikes[];
  types: Object = [{type:"Normal"}, {type:"Electric"}];
  status: Object = [{state:"Available"}, {state:"Busy"}, {state:"Maintenance"}];
  typeSelected: String = "all";
  statusSelected: String = "all";
  initCode: String ="";
  displayMode: String = "33.3%";

  constructor(private  data: BikeService, private  resizer: ResizeService, public dialog: MatDialog, public dialogS: DialogService) {
    resizer.onResize$.subscribe(() => window.innerWidth < 768 ? this.displayBlock() : this.displayFlex());
    if (this.resizer.isMobile) {
      this.displayBlock()
    }
    this.refresh()
  }

  ngOnInit() {

  }

  private refresh() {
    this.data.getBikes().then(res => this.bikes$ = res)
      .catch(err=> console.log("GET bikes: " + err))
  }

  press() {
    const dialogRef = this.dialog.open(DialogAddBikeComponent, {
      minWidth: 100,
      maxWidth: 500,
      width: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.state) {
        this.data.createBike(result.type, result.tower)
          .then(() => {
            this.refresh();
            this.dialogS.openConfirmDialog("Bike created successfully!")
          })
          .catch(err => {
            console.log("POST newBike:" + err);
            this.dialogS.openErrorDialog()
          })
      }
    });
  }

  displayBlock() {
    this.displayMode = "100%"
  }

  displayFlex() {
    this.displayMode = "33.3%"
  }
}
