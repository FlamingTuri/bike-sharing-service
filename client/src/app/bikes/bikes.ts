import {Type} from "./type";

export class Bikes {
  currLocation: {
    "lat": Number,
    "long": Number
  };
  type: Type;
  currState: {
    name: String,
    id: String
  };
  id: String
}
