import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddBikeComponent } from './dialog-add-bike.component';

describe('DialogAddBikeComponent', () => {
  let component: DialogAddBikeComponent;
  let fixture: ComponentFixture<DialogAddBikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddBikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddBikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
