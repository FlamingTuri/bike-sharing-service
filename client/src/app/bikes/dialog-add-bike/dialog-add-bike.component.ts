import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material";
import {BikesComponent} from "../bikes.component";
import {BikeService} from "../bike.service";
import {FreeTowers} from "../../bike-details/freeTowers";

@Component({
  selector: 'app-dialog-add-bike',
  templateUrl: './dialog-add-bike.component.html',
  styleUrls: ['./dialog-add-bike.component.css']
})
export class DialogAddBikeComponent implements OnInit {

  data$: FreeTowers[] = [{type: { id: "", name: ""}, stations: [{ id: "", name: ""}], towers: [{ id: "", name: "", stationId: ""}]}];
  type: FreeTowers = this.data$[0];
  stations: {
    id: String;
    name: String;
  }[] = this.type.stations;
  towers: {
    id: String;
    name: String;
    stationId: String;
  }[] = this.type.towers;
  selectedType: String;
  selectedStation: String;
  selectedTower: String;

  constructor(public dialogRef: MatDialogRef<BikesComponent>,
              private  data: BikeService) { }

  ngOnInit() {
    //getData
    this.data.getFreeTower().then(res => {
      this.data$ = res;
    }).catch(err => console.log("GET freeTower: " + err));
  }

  changeType(value: String) {
    this.selectedType = value;
    this.type = this.data$.find(r => r.type.id == this.selectedType);
    this.stations = this.type.stations;
    this.selectedStation = undefined;
    this.selectedTower = undefined
  }

  changeStation(value: String) {
    this.selectedStation = value;
    this.towers = this.type.towers.filter(t => t.stationId == this.selectedStation);
    this.selectedTower = undefined
  }

  onNoClick() {
    this.dialogRef.close({state: false, type: null, tower:null});
  }

  create() {
    this.dialogRef.close({state: true, type: this.selectedType, tower:this.selectedTower});
  }
}
