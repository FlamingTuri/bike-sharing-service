import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PreviousRouteService} from "../utils/previous-route.service";

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {

  page: number = 0;

  constructor(private route: ActivatedRoute, private routeS: PreviousRouteService) {
    this.route.fragment.subscribe(p => this.page = (p == "stations" || this.routeS.getPreviousRoute().includes("stationDetails"))  ? 1 : this.page)
  }

  ngOnInit() {
  }

}
