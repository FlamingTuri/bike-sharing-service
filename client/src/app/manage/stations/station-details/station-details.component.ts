import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Station} from "../station";
import {StationService} from "../station.service";
import {MatDialog} from "@angular/material";
import {ResizeService} from "../../../utils/resize.service";

@Component({
  selector: 'app-station-details',
  templateUrl: './station-details.component.html',
  styleUrls: ['./station-details.component.css']
})
export class StationDetailsComponent implements OnInit {

  id: String;
  station: Station = {id: "", name: "", type: {name: ""}, location: { lat: 0, long: 0}, towersCount: 0, availableBikes: 0};
  towers: {name:String, bike: String}[] = [{name:"", bike: ""}];
  textToggle: String = "Show towers";
  showLogList: boolean = false;

  constructor(private route: ActivatedRoute,
              private data: StationService,
              public  resizer: ResizeService) {
    this.route.params.subscribe( params =>  this.id = params.id);
    this.data.getStation(this.id).then(res => {
      this.station = res.station;
      this.towers = res.towers;
      this.towers = this.towers.map(t => ({name: t.name, bike: (t.bike != null) ? 'With bike' : 'Empty'}));
      })
  }

  ngOnInit() {
  }

  toggle() {
    this.textToggle = this.showLogList ? "Show towers" : "Show info";
    this.showLogList = !this.showLogList;
  }

}
