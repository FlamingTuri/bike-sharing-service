import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddStationComponent } from './dialog-add-station.component';

describe('DialogAddStationComponent', () => {
  let component: DialogAddStationComponent;
  let fixture: ComponentFixture<DialogAddStationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddStationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddStationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
