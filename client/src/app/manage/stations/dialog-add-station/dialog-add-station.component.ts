import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {GeolocationService} from "../../../utils/geolocation.service";
import {StationService} from "../station.service";
import {Type} from "../../../bikes/type";
import {Marker, MarkerImpl} from "../../../model/marker";
import {Router} from "@angular/router";
import {DialogService} from "../../../utils/genericDialog/dialog.service";

@Component({
  selector: 'app-dialog-add-station',
  templateUrl: './dialog-add-station.component.html',
  styleUrls: ['./dialog-add-station.component.css']
})
export class DialogAddStationComponent implements OnInit {

  markers: Marker[] = [];
  name: String;
  numSlot: Number;
  lat: Number;
  long: Number;
  selectedType: String;
  types$: Type[] = [];
  numTower = new FormControl('', [Validators.required, Validators.pattern("[1-9][0-9]*")]);
  showS: boolean = false;


  constructor(private data: StationService,
              public geolocationService: GeolocationService,
              private router: Router,
              private dialogS: DialogService) { }

  ngOnInit() {
    this.data.getStationTypes()
      .then(res => {
        this.types$ = res;
        this.selectedType = this.types$[0].id;
      })
      .catch(err=> console.log("GET types: "+err));
    this.geolocationService.getCurrentPosition().then((position) => {
      this.lat = position.coords.latitude;
      this.long = position.coords.longitude;
      this.addMarker();
    })
  }

  getErrorMessage() {
    return this.numTower.hasError('required') ? 'You must enter a value' :
      this.numTower.hasError('pattern') ? 'Insert integer value greater less 0' : '';
  }

  canSubmitBeEnabled(): boolean {
    return this.numTower.hasError('required') || this.numTower.hasError('pattern');
  }

  onNoClick() {
    this.goBack();
  }

  create() {
    let body = {
      name: this.name,
      typeId: this.selectedType,
      location: {
        lat: this.lat,
        long: this.long
      },
      availableBikes: 0,
      towersCount: this.numSlot
    };
    this.showS = true;
    this.data.createStation(body)
      .then(() => {
        this.showS = false;
        this.goBack();
        this.dialogS.openConfirmDialog("Station created successfully!")
      })
      .catch(err => {
        console.log("POST newStation: " + err);
        this.showS = false;
        this.dialogS.openErrorDialog()
      })
  }

  updatePosition($event) {
    this.lat = $event.coords.lat;
    this.long = $event.coords.lng;
    this.addMarker();
  }

  addMarker() {
    this.markers = [];
    this.lat = Number(this.lat.toFixed(5));
    this.long = Number(this.long.toFixed(5));
    let tmp = this.types$.filter(t => t.id == this.selectedType);
    let typeName = tmp.length > 0 ? tmp[0].name : "";
    this.markers.push(new MarkerImpl(this.lat.valueOf(), this.long.valueOf(), {type: {name: typeName}}, false, "station"))
  }

  private goBack() {
    this.router.navigate(["/user/manage"], {fragment: "stations"})
  }
}
