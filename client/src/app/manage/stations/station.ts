
export class Station {
  id: String;
  name: String;
  type: {
    name: String;
  }
  location: {
    lat: Number;
    long: Number;
  };
  towersCount: Number;
  availableBikes: Number;
}
