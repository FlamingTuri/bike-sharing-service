import { Injectable } from '@angular/core';
import {HttpOperationsService} from "../../utils/http-operations.service";
import {Station} from "./station";
import {RestApiService} from "../../utils/rest-api.service";
import {Type} from "../../bikes/type";

@Injectable({
  providedIn: 'root'
})
export class StationService {

  constructor(private http:HttpOperationsService, private restApiService: RestApiService) { }

  getStations(): Promise<Station[]> {
    return this.http.getToPromise(this.restApiService.stationsApi.getPath())
  }

  getStation(id: String): Promise<any> {
    return this.http.getToPromise(this.restApiService.stationsApi.addElementToUri(id).getPath())
  }

  getStationTypes(): Promise<Type[]> {
    return this.http.getToPromise(this.restApiService.bikesApi.addElementToUri("types").getPath())
  }

  createStation(station: any) {
    return this.http.postToPromise(this.restApiService.stationsApi.getPath(), station)
  }
}
