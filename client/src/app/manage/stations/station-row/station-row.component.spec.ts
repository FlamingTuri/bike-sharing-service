import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationRowComponent } from './station-row.component';

describe('StationRowComponent', () => {
  let component: StationRowComponent;
  let fixture: ComponentFixture<StationRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
