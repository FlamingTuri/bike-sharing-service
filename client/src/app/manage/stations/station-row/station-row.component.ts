import {Component, Input, OnInit} from '@angular/core';
import {Station} from '../station';

@Component({
  selector: 'app-station-row',
  templateUrl: './station-row.component.html',
  styleUrls: ['./station-row.component.css']
})
export class StationRowComponent implements OnInit {

  @Input() station: Station;

  constructor() { }

  ngOnInit() {
  }

}
