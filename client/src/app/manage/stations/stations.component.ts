import { Component, OnInit } from '@angular/core';
import {ResizeService} from "../../utils/resize.service";
import {StationService} from "./station.service";
import {Station} from "./station";
import {WsService} from "../../utils/ws.service";

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css']
})
export class StationsComponent implements OnInit {

  stations$: Station[] = [];
  types: Object = [{type:"Normal", id:"5be9eb76e7179a27a535e811"}, {type:"Electric", id:"5be9eba4e7179a27a535e88f"}];
  typeSelected: String = "all";
  initCode: String ="";
  displayMode: String = "33.3%";

  constructor(private  resizer: ResizeService,
              private data: StationService,
              private wsService: WsService) {
    resizer.onResize$.subscribe(() => window.innerWidth < 768 ? this.displayBlock() : this.displayFlex());
    if (this.resizer.isMobile) {
      this.displayBlock()
    }
    this.refresh();
    this.wsService.connect()
                  .subscribe(m=> {
                    let msg = JSON.parse(m.data);
                    if(msg.type == 1) {
                      let s = this.stations$.find(s => s.id == msg.data.id);
                      if(s != undefined) {
                        s.availableBikes = msg.data.availableBikes;
                      }
                    }
                  })

  }

  ngOnInit() {

  }

  private refresh() {
    this.data.getStations().then(res => this.stations$ = res)
      .catch(err=> console.log("GET stations: " + err))
  }

  displayBlock() {
    this.displayMode = "100%"
  }

  displayFlex() {
    this.displayMode = "33.3%"
  }
}
