import { Injectable } from '@angular/core';
import { RestApiService } from '../utils/rest-api.service';
import { HttpOperationsService } from '../utils/http-operations.service';
import { Subject }    from 'rxjs';
import { Marker } from '../model/marker';

@Injectable({
  providedIn: 'root'
})
export class MapMarkerService {

  userId: string = JSON.parse(localStorage.getItem("currentUser")).user.id;
  url: string = this.restApiService.stationsApi.getPath();

  private markerSource = new Subject<Marker>();
  marker$ = this.markerSource.asObservable();

  constructor(
    private httpOperationsService: HttpOperationsService,
    private restApiService: RestApiService
  ) { }

  getStations(): Promise<any> {
    return this.httpOperationsService.getToPromise(this.url);
  }

  addMarker(marker: Marker) {
    this.markerSource.next(marker);
  }
}
