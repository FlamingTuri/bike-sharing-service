import { Component, Input, ViewChild, OnInit, Output, EventEmitter } from '@angular/core';
import { AgmMap } from '@agm/core';

import { Location } from '../model/location';
import { Direction } from '../model/direction';
import { Marker } from '../model/marker';
import { GeolocationService } from '../utils/geolocation.service';
import { BottomSheetOperationNotifierService } from '../show-map-details/bottom-sheet/bottom-sheet-operation-notifier.service';

declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() markers: Marker[] = [];
  @Input() userLocation: Marker = null;
  @ViewChild(AgmMap) map: AgmMap;
  @Output() markerClick = new EventEmitter<Marker>();
  @Output() mapClick = new EventEmitter<any>();

  mapLocation: Location;
  initializationDone: boolean = false;

  directions: Direction = {
    directionsOptions: {
      suppressMarkers: true,
    }
  };

  constructor(
    public geolocationService: GeolocationService,
    private operationNotifierService: BottomSheetOperationNotifierService
  ) {
    this.operationNotifierService.destinationDirection$.subscribe(destination => {
      this.displayDirections(this.userLocation, destination);
    });
  }

  ngOnInit() {
    this.mapLocation = {
      lat: 44.1391, // Cesena lat
      lng: 12.24315, // Cesena lng
      markers: this.markers,
      zoom: 20
    };
    this.geolocationService.getCurrentPosition().then((position) => {
      this.mapLocation.lat = position.coords.latitude;
      this.mapLocation.lng = position.coords.longitude;
      this.initializationDone = true;
    }).catch(error => {
      console.log(error);
      this.initializationDone = true;
    });
  }

  onMarkerClick(marker) {
    this.markerClick.emit(marker);
  }

  onMapClick($event) {
    this.mapClick.emit($event);
  }

  displayDirections(origin: Marker, destination: Marker) {
    this.directions.origin = {
      lat: origin.lat,
      lng: origin.lng
    }
    this.directions.destination = {
      lat: destination.lat,
      lng: destination.lng
    }
    console.log(this.directions);
  }
}
