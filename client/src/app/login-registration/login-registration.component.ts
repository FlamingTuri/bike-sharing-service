import { Component, OnInit, OnDestroy } from '@angular/core';
import { ResizeService } from '../utils/resize.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-registration',
  templateUrl: './login-registration.component.html',
  styleUrls: ['./login-registration.component.css']
})
export class LoginRegistrationComponent implements OnInit, OnDestroy {

  isTablet: boolean;
  private subscription: Subscription;

  constructor(private resizeService: ResizeService) { }

  ngOnInit() {
    // console.log(window.location.hostname);
    this.isTablet = this.resizeService.isTablet;
    this.subscription = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
