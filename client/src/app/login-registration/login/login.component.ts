import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ResizeService } from 'src/app/utils/resize.service';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { LocalStorageService } from 'src/app/utils/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  loginFormGroup: FormGroup;
  hide = true;
  errorMsg: string;
  isTablet: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    fb: FormBuilder,
    public resizeService: ResizeService,
    private localStorageService: LocalStorageService
  ) {
    let storedCredentials = localStorageService.getItem("storedCredentials");
    let jsonCredentials;
    if (storedCredentials) {
      jsonCredentials = JSON.parse(storedCredentials);
    }
    let controlsConfig = {
      username: [jsonCredentials ? jsonCredentials.username : "", Validators.required],
      password: [jsonCredentials ? jsonCredentials.password : "", Validators.required],
      checked: false
    }
    this.loginFormGroup = fb.group(controlsConfig);
  }

  ngOnInit(): void {
    // removes the localstorage token if it was present
    this.authService.logout();

    this.isTablet = this.resizeService.isTablet;
    this.subscription = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
    });
  }

  onSubmit() {
    let credentials = {
      username: this.loginFormGroup.value.username,
      password: this.loginFormGroup.value.password
    };

    this.authService.login(credentials).then(result => {
      console.log(result);

      if (this.loginFormGroup.value.checked) {
        this.localStorageService.setItem('storedCredentials', JSON.stringify(credentials));
      }

      this.localStorageService.setItem('currentUser', JSON.stringify(result));

      // probably useless here
      let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/user/home';

      // Set our navigation extras object
      // that passes on our global query params and fragment
      let navigationExtras: NavigationExtras = {
        queryParamsHandling: 'preserve',
        preserveFragment: true
      };

      // Redirect the user
      this.router.navigate([redirect], navigationExtras);
    }).catch(response => {
      this.errorMsg = response.error.message;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
