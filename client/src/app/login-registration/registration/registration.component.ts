import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/auth.service';
import { LocalStorageService } from 'src/app/utils/local-storage.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {

  registrationFormGroup: FormGroup;
  hide = true;
  emailValidator = new FormControl('', [Validators.required, Validators.email]);
  errorMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
    private localStorageService: LocalStorageService
  ) {
    this.registrationFormGroup = fb.group({
      firstname: this.getDefaultRequiredControl(),
      lastname: this.getDefaultRequiredControl(),
      username: this.getDefaultRequiredControl(),
      email: this.emailValidator,
      password: this.getDefaultRequiredControl(),
      passwordConfirmation: this.getDefaultRequiredControl(),
      floatLabel: 'auto',
    });
  }

  getDefaultRequiredControl(): FormControl {
    return new FormControl('', Validators.required)
  }

  getEmailNotValidErrorMessage() {
    return this.emailValidator.hasError('email') ? 'Not a valid email' : '';
  }

  canSubmitBeEnabled(): boolean {
    return this.emailValidator.hasError('required') || this.emailValidator.hasError('email');
  }

  onSubmit() {
    let values = this.registrationFormGroup.value;
    if (values.password.length < 8) {
      this.displayError("Passwords should be at least 8 characters long");
    } else if (values.password == values.passwordConfirmation) {
      let credentials = {
        name: values.firstname,
        surname: values.lastname,
        username: values.username,
        email: values.emailValidator.value,
        password: values.password
      };
      this.authService.register(credentials).then(() => {
        let loginCredentials = {
          username: values.username,
          password: values.password
        };
        this.authService.login(loginCredentials).then(result => {
          this.localStorageService.setItem('currentUser', JSON.stringify(result));

          // probably useless here
          let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/user/home';

          // Set our navigation extras object
          // that passes on our global query params and fragment
          let navigationExtras: NavigationExtras = {
            queryParamsHandling: 'preserve',
            preserveFragment: true
          };

          // Redirect the user
          this.router.navigate([redirect], navigationExtras);
        }).catch(error => {
          this.displayError("unexpected error");
        });
      }).catch(error => {
        this.displayError("username already taken");
      });
    } else {
      this.displayError("passwords did not match");
    }
  }

  displayError(errorMsg: string) {
    this.errorMsg = errorMsg;
  }
}
