export interface NavItem {
  nameDisplayed: string;
  routerLink: string;
  icon?: string;
}
