import { BadgeStatus } from './badge-status';

export interface Badge {
  id: number,
  icon?: any,
  name: string;
  descr?: string;
  achieved: boolean;
}
