export interface PersonalInfo {
  firstname?: string,
  lastname?: string,
  //birthday?: Date,
  country?: string,
  city?: string,
};
