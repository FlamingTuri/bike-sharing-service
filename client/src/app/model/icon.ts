export interface Icon {
  url: string,
  scaledSize: {
    width: number,
    height: number
  }
}