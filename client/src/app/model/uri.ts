export interface Uri {

  getPath(): string;

  addElementToUri(element): Uri;
}

export class UriImpl implements Uri {
  private path: string;

  constructor(path: string) {
    this.path = path;
  }

  getPath(): string {
    return this.path;
  }

  addElementToUri(element): Uri {
    return new UriImpl(this.path + "/" + element);
  }
}
