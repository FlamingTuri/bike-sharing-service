export interface Label {
  color: string; // '#FFFFFF'
  fontFamily?: string;
  fontSize: string; // '14px'
  fontWeight: string; // 'bold'
  text: string; // 'Some Text'
}