export interface Direction {
  origin?: {
    lat: number, 
    lng: number,
    icon?: any, // 'your-icon-url',
    label?: any, // 'marker label',
    opacity?: number // 0.8,
  };
  destination?: { 
    lat: number, 
    lng: number,
    icon?: any, // 'your-icon-url',
    label?: any, // 'marker label',
    opacity?: number // 0.8,
  };
  directionsOptions: {
    suppressMarkers: boolean, // true
  };
}