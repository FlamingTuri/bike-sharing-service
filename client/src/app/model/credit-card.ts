export interface CreditCard {
  name?: string,
  cardNumber?: number,
  expirationDate?: Date,
  cvCode?: number
}
