import { Label } from '../model/label';
import { Icon } from '../model/icon';

export interface Marker {
  lat: number;
  lng: number;
  label?: Label;
  description?: string;
  iconUrl?: Icon;
  type: String;
  showDetails: boolean; //if true redirect to details, if false show direction. default is false
  draggable: boolean;
  data: any;
}

export class MarkerImpl implements Marker {
  lat: number;
  lng: number;
  label?: Label;
  description?: string;
  iconUrl?: Icon;
  type: String;
  showDetails: boolean;
  draggable: boolean;
  data: any;

  constructor(lat: number, lng: number, data: any = "", showDetails: boolean = false, type: String = "", draggable: boolean = false) {
    this.lat = lat;
    this.lng = lng;
    this.data = data;
    this.type = type;
    this.showDetails = showDetails;
    this.draggable = draggable;

    let iconPath: string;
    switch (type) {
      case "station":
        switch (data.type.name) {
          case "Electric":
            iconPath = './assets/images/marker-stationsE.svg';
            break;
          case "Normal":
          default:
            iconPath = './assets/images/marker-stationsN.svg';
        }
        break;
      case "bike":
        switch (data.type.name) {
          case "Electric":
            iconPath = './assets/images/baseline-place-24px-yellow.svg';
            break;
          case "Normal":
          default:
            iconPath = './assets/images/baseline-place-24px-green.svg';
        }
        break;
      default:
        iconPath = './assets/images/baseline-place-24px.svg';
    }
    this.iconUrl = {
      url: iconPath,
      scaledSize: {
        width: 50,
        height: 50
      }
    };
  }
}
