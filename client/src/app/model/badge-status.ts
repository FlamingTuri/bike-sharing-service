export enum BadgeStatus {
  Available,
  Redeemed,
  Unlockable
}
