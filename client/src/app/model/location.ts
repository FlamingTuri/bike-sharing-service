import { Marker } from './marker';

export interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  markers?: Marker[];
}

export class LocationImpl implements Location {
  lat: number = 44.498955; // Bologna lat
  lng: number = 11.327591; // Bologna lng
  viewport?: Object;
  zoom: number = 20;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  markers?: Marker[];
}
