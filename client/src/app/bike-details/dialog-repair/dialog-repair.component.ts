import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BikeDetailsComponent} from "../bike-details.component";

@Component({
  selector: 'app-dialog-repair',
  templateUrl: './dialog-repair.component.html',
  styleUrls: ['./dialog-repair.component.css']
})
export class DialogRepairComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<BikeDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public bikeId: String) {}

  onNoClick(): void {
    this.dialogRef.close({state: false, notes: null});
  }

  ngOnInit(): void {
  }

  onSave(value: string) {
    this.dialogRef.close({state: true, notes: value});
  }
}
