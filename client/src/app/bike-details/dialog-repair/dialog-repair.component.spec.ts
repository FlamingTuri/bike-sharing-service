import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRepairComponent } from './dialog-repair.component';

describe('DialogRepairComponent', () => {
  let component: DialogRepairComponent;
  let fixture: ComponentFixture<DialogRepairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRepairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRepairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
