import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatDialog, MatPaginator, PageEvent} from "@angular/material";
import {DialogRepairComponent} from "./dialog-repair/dialog-repair.component";
import {DialogAvailableComponent} from "./dialog-available/dialog-available.component";
import {BikeService} from "../bikes/bike.service";
import {Bikes} from "../bikes/bikes";
import {Log} from "./log";
import {DialogLogComponent} from "./dialog-log/dialog-log.component";
import {ResizeService} from "../utils/resize.service";
import {DialogService} from "../utils/genericDialog/dialog.service";

@Component({
  selector: 'app-bike-details',
  templateUrl: './bike-details.component.html',
  styleUrls: ['./bike-details.component.css']
})
export class BikeDetailsComponent implements OnInit {

  id:String;
  bike: Bikes = {id: "", currState: {name:"", id:""}, currLocation: {lat:0, long:0}, type:{name:"", id:""}};
  station: any = {name: ""};
  tower: any = {name: ""};
  logs: Log[] = [];
  visibleLog: Log[] = [];
  pageSize = 5;
  pageSizeOptions: number[] = [5];
  textToggle: String = "Show Log";
  showLogList: boolean = false;
  showS: boolean = true;

  @ViewChild(MatPaginator) private mat: MatPaginator;


  constructor(private  data: BikeService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public  resizer: ResizeService,
              public dialogS: DialogService) {
    this.route.params.subscribe( params => this.id = params.id );
    this.refresh()
  }

  ngOnInit() {
  }

  private showSpinner() {
    this.showS = true;
  }

  private refresh() {
    this.data.getBike(this.id).then(res=> {
      this.bike = res.bike;
      this.station = res.station;
      this.tower = res.tower;
      this.showS = false;
      this.logs = res.logs;
      this.visibleLog = this.logs.slice(0, this.pageSize);
      this.mat.firstPage();
    }).catch(err=> console.log("GET bike: " + err))
  }

  repair() {
    const dialogRef = this.dialog.open(DialogRepairComponent, {
      minWidth: 100,
      maxWidth: 500,
      width: '80%',
      data: this.bike.id
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.state) {
        this.showSpinner();
        this.data.startMaintenance(this.bike.id, result.notes)
          .then(() => this.refresh())
          .catch(err => this.error("startMaintenance:" + err))
      }
    });
  }

  available() {
    const dialogRef = this.dialog.open(DialogAvailableComponent, {
      minWidth: 100,
      maxWidth: 500,
      width: '80%',
      data: {id: this.bike.id, type: this.bike.type.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.state) {
        this.showSpinner();
        this.data.endMaintenance(this.bike.id, result.tower)
          .then(() => this.refresh())
          .catch(err => this.error("endMaintenance:" + err))
      }
    });
  }

  private error(log: String) {
    console.log(log);
    this.showS = false;
    this.dialogS.openErrorDialog()
  }

  showLog(log: Log) {
    this.dialog.open(DialogLogComponent, {
      minWidth: 100,
      maxWidth: 400,
      width: '80%',
      data: log
    });
  }

  changeLog(pageEvent: PageEvent) {
    let endIndex = pageEvent.pageSize*pageEvent.pageIndex + pageEvent.pageSize;
    this.visibleLog = this.logs.slice(pageEvent.pageSize*pageEvent.pageIndex, endIndex < this.logs.length ? endIndex : this.logs.length)
  }

  toggle() {
    this.textToggle = this.showLogList ? "Show log" : "Show info";
    this.showLogList = !this.showLogList;
  }
}
