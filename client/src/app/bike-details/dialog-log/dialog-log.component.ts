import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BikeDetailsComponent} from "../bike-details.component";
import {Log} from "../log";

@Component({
  selector: 'app-dialog-log',
  templateUrl: './dialog-log.component.html',
  styleUrls: ['./dialog-log.component.css']
})
export class DialogLogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BikeDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public log$: Log) { }

  ngOnInit() {
  }

  onClose() {
    this.dialogRef.close();
  }
}
