import {Type} from "../bikes/type";

export class FreeTowers  {
  type: Type;
  stations: [
    {
      id: String;
      name: String;
    }
  ];
  towers: [
    {
      id: String;
      name: String;
      stationId: String;
    }
  ]
}
