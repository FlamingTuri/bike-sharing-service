export class Log {
  location: {
    lat: Number,
    long: Number
  };
  name: String;
  descr: String;
  time: String;
  user: {
    username: String;
    id: String;
  };
  state: {
    name: String;
  }
}
