import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {BikeDetailsComponent} from "../bike-details.component";
import {BikeService} from "../../bikes/bike.service";
import {FreeTowers} from "../freeTowers";

@Component({
  selector: 'app-dialog-available',
  templateUrl: './dialog-available.component.html',
  styleUrls: ['./dialog-available.component.css']
})
export class DialogAvailableComponent implements OnInit {

  data$: FreeTowers = {type: { id: "", name: ""}, stations: [{ id: "", name: ""}], towers: [{ id: "", name: "", stationId: ""}]};
  towers: {
    id: String;
    name: String;
    stationId: String;
  }[] = this.data$.towers;
  selectedTower: String;

  constructor(public dialogRef: MatDialogRef<BikeDetailsComponent>,
              @Inject(MAT_DIALOG_DATA) public bike: {id: String, type: String},
              private  data: BikeService) { }

  ngOnInit() {
    //getData
    this.data.getFreeTower().then(res => {
      this.data$ = res.find(r => r.type.id == this.bike.type)
    }).catch(err => console.log("GET freeTower: " + err));
  }

  changeStation(value: String) {
    this.towers = this.data$.towers.filter(t => t.stationId == value);
    this.selectedTower = undefined
  }

  onNoClick() {
    this.dialogRef.close({state: false, tower: null});
  }

  makeAvailable() {
    this.dialogRef.close({state: true, tower: this.selectedTower});
  }
}
