import { Component, OnInit, OnDestroy, Input, AfterViewInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { ResizeService } from '../utils/resize.service';
import { Subscription } from 'rxjs';
import { Marker } from '../model/marker';
import { MatBottomSheet } from '@angular/material';
import { BottomSheetComponent } from './bottom-sheet/bottom-sheet.component';
import { BottomSheetOperationNotifierService } from './bottom-sheet/bottom-sheet-operation-notifier.service';

@Component({
  selector: 'app-show-map-details',
  templateUrl: './show-map-details.component.html',
  styleUrls: ['./show-map-details.component.scss']
})
export class ShowMapDetailsComponent implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked {

  @Input() markers: Marker[];
  @Input() userLocation: Marker;
  @Input() isManager: boolean = false;
  isTablet: boolean;
  isTabletSubscriber: Subscription;
  computeMapHeightSubscriber: Subscription;
  desktopInfoPanelOpened: boolean = false;
  mapHeight = 300;
  selectedMarker: Marker;
  data;
  infoToDisplay: [string, string][] = [];

  constructor(
    public resizeService: ResizeService,
    private bottomSheet: MatBottomSheet,
    private cdRef: ChangeDetectorRef,
    private operationNotifierService: BottomSheetOperationNotifierService
  ) {
    this.isTabletSubscriber = this.resizeService.onResize$.subscribe(() => {
      this.isTablet = this.resizeService.isTablet;
      if (this.isTablet) {
        this.closeInfoPanel();
      }
    });
  }

  ngOnInit() {
    this.isTablet = this.resizeService.isTablet;
  }

  ngAfterViewInit() {
    this.computeMapHeight();
    this.computeMapHeightSubscriber = this.resizeService.onResize$.subscribe(() => {
      this.computeMapHeight();
    });
    this.cdRef.detectChanges();
  }

  ngAfterViewChecked() {
    //this.computeMapHeight();
    //this.cdRef.detectChanges();
  }

  /**
   * compute the height of the map to make it fill the remaining screen
   * to do that it needs the height of the container and the height of the elements above it
   */
  computeMapHeight() {
    try {
      let containerHeight = this.getHtmlSelectorHeight('.container-to-fill');
      // I know, this is shit, but is the fast way
      // verbose but better for reading
      let selector;
      if (this.isManager) {
        // not working as expected
        selector = 'mat-expansion-panel-header';
      } else if (this.isTablet) {
        selector = 'mat-tab-header';
      } else {
        selector = 'app-rent-return-bike';
      }
      // warning not working for manager if mat-expansion-panel-size changes dinamically
      let headerHeight = this.isManager ? 48 : this.getHtmlSelectorHeight(selector);
      // setting some pixel of border
      let bottomBorder = this.isTablet ? 1 : 3;
      this.mapHeight = containerHeight - headerHeight - bottomBorder;
      // only for testing purpose
      // this.mapHeight = 150;
      /*console.log(containerHeight);
      console.log(headerHeight);
      console.log(this.mapHeight);*/
    } catch (e) {
      //console.log(e);
    }
  }

  getHtmlSelectorHeight(selector: string): number {
    let element = document.querySelector(selector);
    let height = parseFloat(getComputedStyle(element).height);
    let marginTop = parseFloat(getComputedStyle(element).marginTop);
    let marginBottom = parseFloat(getComputedStyle(element).marginTop);
    /*console.log(selector);
    console.log(height);
    console.log(marginTop);
    console.log(marginBottom);*/
    return height + marginTop + marginBottom;
  }

  ngOnDestroy() {
    this.isTabletSubscriber.unsubscribe();
    this.computeMapHeightSubscriber.unsubscribe();
  }

  onMarkerClicked(marker: Marker) {
    if (this.userLocation == undefined || JSON.stringify(this.userLocation) != JSON.stringify(marker)) {
      this.selectedMarker = marker;
      this.data = marker.data;
      this.infoToDisplay = [];
      if (marker.type == "station") {
        this.infoToDisplay.push(["Type", this.data.type.name]);
        this.infoToDisplay.push(["Available Bikes", this.data.availableBikes]);
        this.infoToDisplay.push(["Available Towers", this.data.towersCount]);
      } else if (marker.type == "bike") {
        this.infoToDisplay.push(["Type", this.data.type.name]);
        this.infoToDisplay.push(["State", this.data.currState.name]);
      }
      //console.log(this.data);
      //console.log(this.infoToDisplay);
      if (this.isTablet) {
        this.bottomSheet.open(BottomSheetComponent, {
          data: { marker: marker, infoToDisplay: this.infoToDisplay },
        });
      } else {
        this.desktopInfoPanelOpened = true;
      }
    }
  }

  closeInfoPanel() {
    this.desktopInfoPanelOpened = false;
  }

  showDirections() {
    this.operationNotifierService.displayDirection(this.selectedMarker);
  }

  openInMaps() {
    let url = 'https://maps.google.com/?q=';
    url += this.selectedMarker.lat + ',' + this.selectedMarker.lng;
    /* open the url in a new blank page */
    window.open(url, '_blank');
  }
}
