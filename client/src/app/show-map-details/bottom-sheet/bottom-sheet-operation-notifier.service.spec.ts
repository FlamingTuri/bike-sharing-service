import { TestBed, inject } from '@angular/core/testing';

import { BottomSheetOperationNotifierService } from './bottom-sheet-operation-notifier.service';

describe('BottomSheetOperationNotifierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BottomSheetOperationNotifierService]
    });
  });

  it('should be created', inject([BottomSheetOperationNotifierService], (service: BottomSheetOperationNotifierService) => {
    expect(service).toBeTruthy();
  }));
});
