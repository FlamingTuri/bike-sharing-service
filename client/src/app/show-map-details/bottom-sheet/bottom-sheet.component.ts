import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { Marker } from '../../model/marker';
import { ResizeService } from '../../utils/resize.service';
import { Subscription } from 'rxjs';
import { BottomSheetOperationNotifierService } from './bottom-sheet-operation-notifier.service';

@Component({
  selector: 'app-bottom-sheet',
  templateUrl: './bottom-sheet.component.html',
  styleUrls: ['./bottom-sheet.component.css']
})
export class BottomSheetComponent implements OnInit, OnDestroy {

  marker: Marker;
  infoToDisplay: [string, string][];
  subscription: Subscription;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private bottomSheetRef: MatBottomSheetRef<BottomSheetComponent>,
    private resizeService: ResizeService,
    private operationNotifierService: BottomSheetOperationNotifierService
  ) {
    console.log(data);
    this.marker = data.marker;
    this.infoToDisplay = data.infoToDisplay;
  }

  ngOnInit() {
    this.closeInDesktopMode();
    this.subscription = this.resizeService.onResize$.subscribe(() => {
      this.closeInDesktopMode();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  closeInDesktopMode() {
    if (!this.resizeService.isTablet) {
      this.closeBottomSheet();
    }
  }

  closeBottomSheet() {
    this.bottomSheetRef.dismiss();
  }

  showDirections(){
    this.operationNotifierService.displayDirection(this.marker);
  }

  openInMaps() {
    let url = 'https://maps.google.com/?q=';
    url += this.marker.lat + ',' + this.marker.lng;
    /* open the url in a new blank page */
    window.open(url, '_blank');
  }
}
