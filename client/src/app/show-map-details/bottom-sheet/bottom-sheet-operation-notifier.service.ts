import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Marker } from '../../model/marker';

@Injectable({
  providedIn: 'root'
})
export class BottomSheetOperationNotifierService {

  private destinationDirectionSource = new Subject<Marker>();

  destinationDirection$ = this.destinationDirectionSource.asObservable();

  displayDirection(marker: Marker) {
    this.destinationDirectionSource.next(marker);
  }
}
