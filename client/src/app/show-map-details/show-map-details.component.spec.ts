import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMapDetailsComponent } from './show-map-details.component';

describe('ShowMapDetailsComponent', () => {
  let component: ShowMapDetailsComponent;
  let fixture: ComponentFixture<ShowMapDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowMapDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMapDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
