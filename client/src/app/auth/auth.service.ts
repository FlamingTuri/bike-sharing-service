import { Injectable } from '@angular/core';
import { RestApiService } from '../utils/rest-api.service';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpErrorHandlerService } from '../utils/http-error-handler.service';
import { HttpOperationsService } from '../utils/http-operations.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(
    public httpOperationsService: HttpOperationsService
  ) { }

  login(credentials): Promise<any> {
    let url = this.httpOperationsService.restApiService.loginApi.getPath();
    return this.httpOperationsService.postToPromise(url, credentials, false);
  }

  register(credentials): Promise<any> {
    let url = this.httpOperationsService.restApiService.registrationApi.getPath();
    return this.httpOperationsService.postToPromise(url, credentials, false);
  }

  logout(): void {
    if (localStorage.getItem('currentUser')) {
      localStorage.removeItem('currentUser');
    }
  }

}
