import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../utils/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SuperUserGuard implements CanActivate, CanActivateChild {

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.isSuperUser("");
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    return this.canActivate(childRoute, state);
  }

  isSuperUser(url: string): boolean {
    if (this.localStorageService.isAdmin) {
      return true;
    } else {
      // Store the attempted URL for redirecting
      //this.authService.redirectUrl = url;

      // Navigate to the login page with extras
      this.router.navigate(['/authentication/login']);
      return false;
    }
  }
}
