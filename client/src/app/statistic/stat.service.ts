import { Injectable } from '@angular/core';
import {HttpOperationsService} from "../utils/http-operations.service";
import {RestApiService} from "../utils/rest-api.service";

@Injectable({
  providedIn: 'root'
})
export class StatService {

  constructor(private http:HttpOperationsService, private restApiService: RestApiService) { }

  getTotCount(): Promise<{count: Number}> {
    return this.http.getToPromise(this.restApiService.statsApi.addElementToUri("total-rent-count").getPath())
  }

  getStatsForType(): Promise<{type: String, averageTime: number, count: Number}[]> {
    return this.http.getToPromise(this.restApiService.statsApi.addElementToUri("average-rent-time-by-type").getPath())
  }

  getRanking(): Promise<{name: string, count: number}[]> {
    return this.http.getToPromise(this.restApiService.statsApi.addElementToUri("stations-usages-count").getPath())
  }

  getTrend(): Promise<{_id: number, monthlyUsage: {typeName: String, count: number}[]}[]> {
    return this.http.getToPromise(this.restApiService.statsApi.addElementToUri("monthly-rent-count-by-type").getPath())
  }
}
