import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from "@angular/material";
import {StatService} from "./stat.service";
import {RankingElement} from "./rankingElement";
import {Chart} from "chart.js";



const toHour = 1000 * 60 *60;

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  //ranking
  displayedColumns: string[] = ['position', 'name', 'num'];
  dataSource = new MatTableDataSource([]);
  //report
  tot: Number = 0;
  normal: Number = 0;
  elec: Number = 0;
  avgN: Number = 0;
  avgE: Number = 0;
  //trend
  label: String[] = ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Jan'];
  yN: number[] = [0,0,0,0,0,0,0,0,0,0,0,0];
  yE: number[] = [0,0,0,0,0,0,0,0,0,0,0,0];
  lineChart = [];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private data: StatService) {

  }

  ngOnInit() {
    let pTot = this.data.getTotCount();
    let pType = this.data.getStatsForType();
    let pRanking = this.data.getRanking();
    let pTrend = this.data.getTrend();

    Promise.all([pTot, pType, pRanking, pTrend]).then(res => {
      this.tot = res[0].count;

      //stat for type
      res[1].forEach(type => {
        if (type.type == 'Normal') {
          this.normal = type.count;
          this.avgN = Math.trunc(type.averageTime / toHour);
        } else if (type.type == 'Electric') {
          this.elec = type.count;
          this.avgE = Math.trunc(type.averageTime / toHour);
        }
      });

      //ranking
      let rank = res[2].filter((v,i) => i < 5)
                       .map((r, i) => new RankingElement(i+1, r.name, r.count));
      this.dataSource = new MatTableDataSource(rank);

      //trend
      res[3].forEach(e => {
        e.monthlyUsage.forEach(t => {
          if (t.typeName == 'Normal') {

            this.yN[e._id == 1 ? 11 : e._id-2] = t.count
          } else if (t.typeName == 'Electric') {
            this.yE[e._id == 1 ? 11 : e._id-2] = t.count
          }
        })
      });
      this.createChart();
    })
  }

  private createChart() {
    this.lineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: this.label,
        datasets: [
          {
          label: "Normal bike rents",
          data: this.yN,
          fill: false,
          lineTension: 0.2,
          borderColor: "green",
          backgroundColor: "green",
          borderWidth: 2,
        },
        {
          label: "Electric bike rents",
          data: this.yE,
          fill: false,
          lineTension: 0.2,
          borderColor: "orange",
          backgroundColor: "orange",
          borderWidth: 2,
        }]
      },
      options: {
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Num of rents'
            }
          }]
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    })
  }

}
