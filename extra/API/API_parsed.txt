Client:

POST /register --> create a new user [TESTED]
POST /login --> user login [TESTED]

GET /users/:id --> get user info by id [TESTED]
PUT /users/:id --> update user info [TESTED]
GET /users/:id/bike --> get bike rent by user [TESTED]

POST /towers/:id/rent-bike --> rent a bike [OK]
POST /towers/:id/return-bike --> return a bike [OK]

GET /stations --> get all stations [TESTED]

Manager:

GET /users --> get users [TESTED]

GET /bikes --> get all bikes [OK]
GET /bikes/:id --> get a bike [OK]
POST /bikes --> creates a new bike [OK]
POST /bikes/positions --> create ws to track positions [OK]

GET /towers --> get all towers [OK]
GET /towers?free --> get all free towers [OK]

POST /bikes/:id/start-maintenance --> repair bike [OK]
POST /bikes/:id/end-maintenance --> make the bike available again [OK]

GET /stations --> get all stations [TESTED]
GET /stations/:id --> get station [TESTED]
GET /stations?type=BIKE_TYPE_ID --> get stations by type id [TESTED]
POST /stations --> creates a new station [TESTED]

GET /bikes/types --> get all possible types of (bike == towers == stations) [OK]